��    8     �  O  �3      �D  �   �D  X   �E  8  #F  �   \G  �   �G     �H  �   �H    �I  ?   �J  �   �J     bK     pK     ~K  a  �K  �  �P  0  �R  Q  V  �  WW    �X  ;   Z  n   <]  �   �]  �   <^  S   �^    H_  /  Pb  �  �c  +  re  �   �f     Ug  E   [g  f  �g  4   k  H   =k  "   �k  
   �k     �k     �k  	   �k  	   �k     �k     �k  $   �k  "   l  i   9l  X   �l  �   �l  '   �m  2   �m      n     6n     In     ^n     sn     �n  	   �n  
   �n  J   �n     �n  
   o  	   o     o  Q   o  X   oo  Y   �o  1   "p  &  Tp     {r  *   �r  �   �r  G   �s  N   �s  8   t  @   St     �t     �t     �t     �t     �t  	   �t     �t  	   �t  
   �t     �t  !   u     -u  	   5u     ?u     Nu     ^u     gu     ou  C   |u      �u     �u     �u     �u     v     v     v     v  
   %v     0v     9v     Hv     bv     uv     |v     �v  
   �v  �   �v     ?w     Nw     Ww  	   qw     {w     �w     �w  �   �w  �   ox  �   y  P   �y     ;z     Mz     Vz     pz     �z  R   �z  �   �z     �{     �{     �{  c   �{     I|     U|  Q   p|  �   �|  \   h}  _   �}  Z   %~  
   �~  !   �~  
   �~  ^   �~  �        �     �     �  
   �  	   �     �     
�     %�  Z   .�     ��     ��     ��     ��  	   ��  =   Ȁ  	   �     �  �   ,�  (    �  (   I�  7   r�  7   ��  F   �  C   )�  4   m�  1   ��  6   ԃ  ?   �  A   K�  3   ��     ��  	   ʄ     Ԅ     ۄ     ݄     �     �  y   ��     s�     ��     ��     Ѕ     ׅ     �     ��     ��     �  	   ,�  �   6�  �   ��  _   ��  t   �  0   Z�     ��     ��  Y   ��     ��     �  !   2�     T�     `�     |�     ��     ��     ��     ��     ��  3   ԉ  6   �  6   ?�  .   v�     ��     ��  +   Ɗ     �     �  �   
�     ��     ��     ��     ԋ     ݋  4   ��  P   ,�  P   }�  <   Ό  /   �     ;�  �  Y�  �   >�  �   ��  l   ��  &   ��  S   �  �   r�  �   �  �   ��  �   H�  *   �     �  �   (�  �   ��  e   1�  V   ��  4   �  �   #�     �  
   	�  	   �     �     8�     @�     P�  �   W�     �     �     �     "�     4�     @�  $   P�  %   u�  2   ��  %   Ι  "   ��     �     +�     C�     T�     h�     x�     ��     ��     ʚ     ߚ  (   �  (   �     E�     W�     ]�     f�  G   o�  	   ��     ��  *   ϛ  (   ��  (   #�  	   L�     V�     _�     k�  _   {�     ۜ      �     �     �  	   �     $�     ,�     E�     N�     m�     ��  	   ��     ��     ��  	   ��     ��  
   ǝ     ҝ     �     ��     �  	   �  
   �     *�     =�     S�     [�     `�     n�     }�     ��  
   ��     ��  
   ��     ��     ��     Ǟ     ֞     ߞ  
   ��     �     
�     �  r   �    ��     ��     ��     ��     ��     Ѡ     �     �  \   �     w�      ��     ��     ֡  	   ��  
   ��     
�     $�     )�  7   5�     m�     z�     ��     ��     ��     ��     ΢  ,   ۢ  7   �  
   @�     K�     T�     j�     ��     ��     ��     ��     ˣ  	   ѣ     ۣ     ��  	   �     �     %�     ?�  	   H�     R�     Z�     l�     t�     ��     ��     ��  	   ��     ��     ��  	   ��     ��     ʤ     Ф     פ     �     �     ��     �     �     �     !�     4�     =�     I�  
   N�  -   Y�     ��     ��     ��  	   ��     ��  
   ��     ̥     ե     ۥ     �  	   �     ��     ��     �     �     �  
   3�  
   >�     I�  	   d�     n�     ~�     ��     ��     ��     ��     ��     ͦ     Ϧ     Ԧ     ۦ     �     �     �     �     �     �  (   $�     M�  6   ^�  /   ��  8   ŧ     ��     
�     "�     '�  
   6�     A�     T�     g�     i�  
   l�  �   w�     �  �   *�  �   ��  �   :�  7   ê  3   ��     /�     6�  5   >�  r   t�     �     �  d   %�     ��     ��     ��     ��     ��     ��     ��     Ŭ     ج  �   �     u�     ��     ��  �   ��  }   #�     ��     ��     ��     Ү     �     �  "   	�     ,�     G�  1   b�  !   ��     ��      ѯ  %   �  3   �  "   L�  r   o�     �     �     �     �     +�     7�  0  D�     u�  1   }�     ��     ��     ز     �     ��  
   �  W   "�  1   z�  >   ��  Z   �  8   F�  H   �  �   ȴ  ,   Q�  
   ~�     ��  "   ��     ��     ��     ��  	   ȵ     ҵ     ׵     ٵ  6   ݵ  7   �     L�     ]�     s�  
   ��  }  ��     �  
    �  	   +�     5�     <�     D�     V�     f�     l�  	   t�     ~�     ��     ��     ��     ��  '   ��  	   ޸     �  
   ��     �     �     )�     1�  	   @�  
   J�     U�     Z�     i�     q�     y�     ��     ��     ��     ��     ʹ     ۹     �     ��     �     &�     8�     F�     V�  _   q�     Ѻ     �     ��  V   �  ;   i�  '   ��     ͻ     �     �  a   �  J   h�     ��     ¼     ɼ     ׼     �  
   �     ��     �     #�  
   +�     6�     ;�     V�  
   ^�     i�     ��     ��     ��     ��     ��     ɽ     �     ��     �     !�     *�     D�     K�     [�     d�     k�  %   y�  
   ��     ��     ��     þ     ̾     �  	   �     ��      �     �     �     �     �      �     %�  	   .�     8�     T�     m�     ��     ��     ��     ��  "   ��  #   	�     -�     H�  !   d�     ��     ��  x   ��  �   �  h  ��  3   '�    [�  �   p�  �   M�  �   �  �  ��     ��  3   	�     =�  j   R�  )   ��  #   ��  6   �  `  B�  B   ��     ��  4   �     6�    Q�  =   X�     ��  	   ��     ��     ��     ��     ��  S   ��     &�     /�     5�     :�  �   N�     %�     -�     ?�     H�     Q�     V�  *  t�  1  ��  ;   ��  /   �  �   =�     ��  �  ��     H�     J�     O�  
   W�  W   b�  \   ��  I   �  
   a�     l�     q�    ��     ��     ��     ��  G   ��  �   ��  5   ��  �   ��  �   ��  �   I�     ��  	   ��  	   ��     ��     ��     �     �     #�     ,�     5�     G�     N�  �  [�     O�     \�     n�     ��  
   ��     ��     ��     ��     ��     ��     ��     ��     ��     
�  C   �  �   V�     �     �     �     #�     ,�  	   D�     N�     R�     o�     {�  9   ��  4   ��     ��     �     �     7�     Q�     `�     m�     y�     ��  &   ��     ��  %   ��     �  !   '�     I�  )   d�     ��  $   ��     ��     ��     �     #�     ?�  (   Z�  %   ��  #   ��  %   ��     ��      �     4�  !   O�     q�     ��     ��      ��     ��  #   �      +�  "   L�  "   o�      ��     ��     ��  W   ��     �     ?�  �  D�  �   ��  H   ��  �   �  a   ��  	  W�     a�  �   u�     �  A   ?�  y   ��     ��     �      !�  `  B�  �  ��  �  1�  =  ��  �  ��  �   ��  �  ��  s   ��  v     �   �  J   J z  � �    �  � �   � �   �    % 4   +   ` 4   e G   � !   � 	                    .    <    O (   \ '   � `   � M    �   \ '   2 0   Z    �    �    �    �    �            0 `   ?    � 
   � 	   � 
   � E   � L    M   Y K   � �  �    � !   � �    4   � ?    @   O H   � 
   �    �    �    �     	    	       %    . ,   B !   o    �    �    �    �    �    �    � K   � '   )    Q    ] 	   e 	   o    y    � 
   � 
   � 
   �    �    �    �    � '   � 
       " |   1    �    �    � 	   �    �    �    
 �     �   � �   � b   V    �    �    �    �     N    �   k    , 
   ;    F o   \    � !   � X   � �   V �    H   � _       h    v    � V   � �   �    �     �     �     �      !    !     !! 
   B! V   M!    �!    �!    �!    �! 	   �! Q   �! 
   7" "   B" �   e" *   <# *   g# 6   �# 9   �# H   $ E   L$ 6   �$ 3   �$ 8   �$ A   6% C   x% 5   �%    �% 
   �%    	&    &    &    &    "& v   4& !   �& !   �& !   �&    '    '    *'    9'    H'    X' 	   p' �   z' �   ( O   �( �   �( 3   h)    �)    �) U   �)    *    "*    ?*    ^* #   o*    �*    �*    �*    �* #   �*    �* :   + =   =+ =   {+ 4   �+    �+    �+ :   ,    N,    ^, �   f,    -    -     -    ;-    D- -   d- @   �- @   �- :   . 9   O.    �. �  �. �   �1 ~   72 Z   �2    3 T   +3 �   �3 �   "4 �   �4 |   i5    �5     6 z   6 �   �6 Q   7 Y   f7 0   �7 �   �7    �8 
   �8 
   �8    �8    �8    �8 	   9 �   9 )   �9    �9    �9    �9 
   :    :     : "   >: .   a: !   �: #   �:    �:    �:    ;    ;    /;    A;    Z;    u;    �;    �;     �; !   �;    �;    <    < 	   !< K   +< 
   w<    �< )   �< *   �< 6   �<    =    /=    ;=    H= q   X= 
   �= &   �= 	   �=    >    > 	   &>     0>    Q> '   Z> 	   �>    �> 	   �>    �>    �>    �>    �>    �>    �> 	   ?    ? 
   +? 
   6? 
   A?    L?    d?    |?    �?    �?    �?    �?    �? 
   �?    �? 
   �?    �?    �?    @     @    )@ 
   G@    R@    Z@    m@ �   r@ �   �@    �A    �A    �A    �A )   B !   ;B    ]B ^   qB *   �B +   �B &   'C %   NC    tC    �C    �C    �C    �C 5   �C    �C    �C    D 
   #D    .D    GD    TD -   aD )   �D 
   �D    �D !   �D &   �D 	   E #    E    DE    ME    mE 	   sE     }E    �E    �E    �E    �E    �E 	   �E 	   �E    F    F     F 	   ,F 	   6F    @F    HF    QF    ZF 
   cF    nF    vF    }F 	   �F    �F    �F 	   �F 	   �F 	   �F 
   �F    �F    �F 
   �F    �F    G 6   G    IG    MG !   YG 
   {G 
   �G    �G    �G    �G    �G 	   �G    �G 
   �G    �G    �G 
   �G    H 
   $H 
   /H    :H    RH    cH    ~H    �H    �H    �H    �H    �H    �H    �H    �H    I    I    "I    3I    <I    EI 	   TI .   ^I    �I D   �I 9   �I D   #J    hJ $   uJ 
   �J    �J 
   �J    �J    �J    �J    �J 	   �J �   �J    �K �   �K �   0L �   �L X   CM T   �M    �M    �M 5    N _   6N    �N    �N [   �N    *O    0O 	   9O    CO 
   KO    VO    XO    `O     yO t   �O     P    0P 	   8P y   BP �   �P    @Q    QQ    ZQ    oQ    �Q    �Q $   �Q #   �Q #   �Q 6   R -   NR "   |R !   �R (   �R /   �R "   S �   =S 	   �S     �S     �S 	   T    T    $T   6T 
   DU @   OU    �U    �U    �U    �U    �U    �U T   	V *   ^V /   �V 2   �V +   �V 2   W    KW *   �W    �W    X $   X    2X    4X    ;X    CX    LX    RX    TX -   XX .   �X    �X    �X 	   �X    �X Y  �X    TZ 	   jZ 
   tZ    Z    �Z 	   �Z    �Z    �Z    �Z    �Z    �Z 	   �Z    �Z    �Z    �Z 2   [ 	   8[    B[    X[    f[    �[    �[    �[ 
   �[ 
   �[    �[    �[    �[    �[    �[    �[    \    \    2\    H\    [\    p\    �\    �\    �\    �\    �\    �\ R   
] !   ]]    ]    �] E   �] *   �] ,   &^    S^    j^    s^ `   �^ K   �^    0_    B_    J_    ]_    q_    �_    �_    �_    �_    �_    �_    �_    �_    ` "   `    0`    7` 	   @`    J`    [`    r`    �` "   �`    �` 
   �` !   �` 	   
a    a    +a    1a    8a #   Ia 	   ma    wa    �a    �a    �a    �a 	   �a    �a    �a 	   �a 	   �a z  �a    qd    yd    �d    �d    �d     �d    �d    �d    e    "e    @e    ]e    }e    �e    �e    �e     �e    f _   f �   pf s  �f 1   nh '  �h �   �i �   ~j �   k e  �k    1m +   Nm    zm _   �m    �m #   n 1   2n W  dn :   �o !   �o I   p    cp   |p 5   �q    �q 
   �q    �q    �q 	   �q 
   �q D   r    Mr    Ur    Zr    `r �   wr    Cs    Ls    as 
   js    us     |s �   �s �   �t -   su ,   �u �   �u    Xv �  ^v    �w    �w    �w 	   �w c   �w f   ]x H   �x    y    y    'y �   By    z    "z    *z R   0z r   �z 3   �z �   *{ �   �{ h   d|    �| 
   �|    �| 
   �| 
   �| 
   }    } 	   }    !}    *}    ;}    B} �  O}            /    G 
   W    b    k    �    �    �    �    �    �    � C   � �   >�    ��    �    � 	   #�    -� 	   J�    T� *   X�    ��    �� B   �� >   �    $� !   0�    R�    k�    ��    ��    �� 
   �� "   Ƃ &   �    � #   0�    T� $   s�    �� -   ��    � &   �    ,�    K�    h�    ��    �� )   �� %   � )   � (   :�    c� %   ��    �� "   Ņ    � $   �    ,� +   J�    v� #   �� )   �� '   �� &   � "   /�    R�    _� X   n� $   Ǉ    �      �     �      M  *  �      C   �   U   �   I  �   �                �         �  <   ^   n         �  /  �  �       $  �    �       �      4    �   �     d      E  B      �  �       �  �   �   �       #      �  -                 �   �       N   �  3    q   f  �   �  �  �    Y  5    �  �  Y           x     u      J      (  �      Q  �      �  �      �  �      ~  �    i           �  �    p      �  d        �     �  c            �     �  e  �      )      r  q  �     V          �   �   @  �   �      !  *   ]    �      K   �             �          �              �  �    &        �   '      �   D   �         f          }      �       �  �  �  �            �      U  >      �      �   �   #              �  G       ;   e   g  �   ?     �  m  F   �   1   �  �    �  �   �       �         �      �          �  ?  �   �  �    X       �  �  �   �  �  %       9  _   �        �   �          �     �        �  	       �  �  X  w  %      �              v   �       =  �  g     �   %      8       
   �              r            Z  �  j  {      �  �   n  �      E      �       &   '  o  �  :        �  �  +  �   �      �   �          y              ?  j    !  �   �   h      H  v      �         �      #  8  3  9          �  ;  h   �   �  H  �  �  G     t           f   �  !   �   �  &  R  2  &  p          �   �     �  �                  �   L         �  �     X         �   z  ~  �  �  4  �      �   �  �  T  m   �   �      @   	             �   C  D  �  ^  �  �        0  e      �  `  "  �          �   :       g  t  O  �  �      �        '  �  "   
  �   R  �      �  �     S       �  �      W                     �      �      .  6  @  m      p   I  �       �  s   �   �  �  R     �  �  �  P  N  �   �  �   "  v      �   �        s      J       +       |   S  .      �   �      S  6  -  �              �   �      �  �          l   �          7  �        Y  \  i          `  -  �  a  a          �      �  k       B                             T   _  0  �  �    N  �       �      u   �  �   =      �  B      �   x      |          �           �   *      T      5  �         �  �   �  �  �   ,  P  ~   �  �   �   .    �  �  �  �   i  $  +        �  �    �  )   �   �  �              �  F  E   �   �       �  ]             �      �      0       �  �   <  �   �  7  �      0          �  �   �         :  �  t  �      �   �           �   H   �      �   �  �   �      O         w  �  �  �                       �  �  �  r      w       =       �   �   �      J  W              �   j   �  z      �  [   2  O      D  4   �  /       ,  "  F           �            �          ,   �  �                   �   6  l    \   {   y  �  �  �  �    .   |  �  �   �      >      �      5  �   {          �   �          �  l  �      Z      �  W  M   �  5   �  c     7       �      �  �      	    �      �       �      4  �        �      I   !  �  �  1        ;             �   �  �        �   V     �      >      c  [          �       /      x  �  �       �   V           �      �    �                 M      (   �  h  K  �       �  3   }   K  u          �   A      q  \    �   8  �   �      �  �  �  �  $      +  �  �  A      s  8  �      �  )  �  �  _  1  C  �  �       �     
  �   Q          �  �  $               /  *  �   �      �  �       �  #  �           �    �  n  �      �                 �  �  �  �  �  �  �   �      �  �    �   d           b   k         %  (      �             ]          b  (  �  �   2  -   L  �      �     �   �        �  �        �  �  �   �   �   
      �   2           U  <  �   �  Q  '   P   �      9   �  o  `       [  �          �   �   �  �      	  �    y   �  �   �  �  �  �  �            �   L          �   �      �   ,  1  �  Z  b  z  �  a   }  6   ^      )  �  G  �     �  3      �       A  o   k      7   
                 Typically, Date will be associated with the creation or
                 availability of the resource. Recommended best practice for
                 encoding the date value is defined in a profile called
                  
                 and includes (among others) dates of the form YYYY-MM-DD.
             
                Typically, Subject will be expressed as keywords, key phrases or
                classification codes that describe a topic of the resource.
                Recommended best practice is to select a value from a controlled
                vocabulary or formal classification scheme.
              
              To describe the physical or digital manifestation of the resource,
              use the FORMAT element.
             
              Type includes terms describing general categories, functions, or
              genres, for content. Recommended best practice is to select a value
              from a controlled vocabulary or controlled classification scheme.  
                   <p>If the applet you're adding was generated 
by one of the programs in this drop down, please select it, 
then add the data/applet file generated by your program.  <p>If the applet you're adding was generated 
by one of the programs in this drop down, please select it, 
then add the data/applet file generated by your program. </p>
<p>eg. For Geogebra applets, select geogebra, then add the .ggb file that 
you created in Geogebra.</p> "%s" already exists.
Please try again with a different filename "Find the .txt file (in the applet file) 
and open it. Copy the contents of this file <ctrl A, ctrl C> into the applet 
code field. (Afan) Oromo  ---Move To--- . Please use ASCII names. <dl>  <dt>If your goal is to test understanding of core concepts or reading comprehension  </dt>  <dd>    <p>  Write a summary of the concept or reading long  enough to adequately test the target's knowledge, but short enough not to induce fatigue. Less than one typed page is probably adequate, but probably considerably less for young students or beginners.    </p>    <p>Select words in the text thatare key to understanding the concepts. Thesewill probably be verbs, nouns, and key adverbs.Choose alternatives with one clear answer.    </p>  </dd>  <dt>If your goal is to test vocabulary knowledge  </dt>  <dd><p>Write a text using the target vocabulary. This text should be coherent and cohesive, and be of an appropriate length. Highlight the target words in the text. Choose alternatives with one clear answer.</p>  </dd>  <dt>If your goal is to test word formation/grammar:  </dt>  <dd>  <p>Write a text using the target forms. This text should be coherent and cohesive, and be of an appropriate length. Remember that the goal is not vocabulary knowledge, so the core meanings of the stem words should be well known to the students.  </p>  <p>Highlight the target words in the text. Provide alternatives with the same word stem, but different affixes. It is a good idea to get a colleague to test the test/exercise to make sure there are no surprises!  </p>  </dd></dl> <ol>  <li>Click &lt;Select an MP3&gt; and browse to the MP3       file you wish to insert</li> <li>Click on the dropdown menu to select the position        that you want the file displayed on screen.</li>  <li>Enter an optional caption for your file.</li> <li>Associate any relevant text to the MP3 file.</li> <li>Choose the type of style you would like the iDevice to       display e.g. 'Some emphasis' applies a border and icon to the iDevice content displayed.</li></ol> <p>
The image with text iDevice can be used in a number of ways to support both
the emotional (affective) and learning task (cognitive) dimensions of eXe
content. 
</p><p>
<b>Integrating visuals with verbal summaries</b>
</p><p>
Cognitive psychologists indicate that presenting learners with a
representative image and corresponding verbal summary (that is presented
simultaneously) can reduce cognitive load and enhance learning retention.
This iDevice can be used to present an image (photograph, diagram or
graphic) with a brief verbal summary covering the main points relating to
the image. For example, if you were teaching the functions of a four-stroke
combustion engine, you could have a visual for each of the four positions of
the piston with a brief textual summary of the key aspects of each visual.
</p> <p>Assign a label for the attachment. It is useful to include the type of file. Eg. pdf, ppt, etc.</p><p>Including the size is also recommended so that after your package is exported to a web site, people will have an idea how long it would take to download this attachment.</p><p>For example: <code>Sales Forecast.doc (500kb)</code></p> <p>Cloze exercises are texts or sentences where students must fill in missing words. They are often used for the following purposes:</p><ol><li>To check knowledge of core course concepts (this could be a pre-check, formative exercise, or summative check).</li><li>To check reading comprehension.</li><li>To check vocabulary knowledge.</li><li>To check word formation and/or grammatical competence. </li></ol> <p>Enter the text for the cloze activity in to the cloze field 
by either pasting text from another source or by typing text directly into the 
field.</p><p> To select words to hide, double click on the word to select it and 
click on the Hide/Show Word button below.</p> <p>If left unchecked a small number of spelling and capitalization errors will be accepted. If checked only an exact match in spelling and capitalization will be accepted.</p><p><strong>For example:</strong> If the correct answer is <code>Elephant</code> then both <code>elephant</code> and <code>Eliphant</code> will be judged <em>"close enough"</em> by the algorithm as it only has one letter wrong, even if "Check Capitilization" is on.</p><p>If capitalization checking is off in the above example, the lowercase <code>e</code> will not be considered a mistake and <code>eliphant</code> will also be accepted.</p><p>If both "Strict Marking" and "Check Capitalization" are set, the only correct answer is "Elephant". If only "Strict Marking" is checked and "Check Capitalization" is not, "elephant" will also be accepted.</p> <p>If this option is checked, submitted answers with different capitalization will be marked as incorrect.</p> <p>If this option is set, each word will be marked as the 
learner types it rather than all the words being marked the end of the 
exercise.</p> <p>Select symbols from the text editor below or enter LATEX manually to create mathematical formula. To preview your LATEX as it will display use the &lt;Preview&gt; button below.</p> <p>Si esta opci&oacute;n esta marcada se muestra la puntuaci&oacute;n obtenida.</p> <p>Teachers should keep the following in mind when using this iDevice: </p><ol><li>Think about the number of different types of activity planned for your resource that will be visually signalled in the content. Avoid using too many different types or classification of activities otherwise learner may become confused. Usually three or four different types are more than adequate for a teaching resource.</li><li>From a visual design perspective, avoid having two iDevices immediately following each other without any text in between. If this is required, rather collapse two questions or events into one iDevice. </li><li>Think about activities where the perceived benefit of doing the activity outweighs the time and effort it will take to complete the activity. </li></ol> <p>The Reading Activity will primarily 
be used to check a learner's comprehension of a given text. This can be done 
by asking the learner to reflect on the reading and respond to questions about 
the reading, or by having them complete some other possibly more physical task 
based on the reading.</p> <p>The Wikipedia iDevice allows you to locate 
existing content from within Wikipedia and download this content into your eXe 
resource. The Wikipedia Article iDevice takes a snapshot copy of the article 
content. Changes in Wikipedia will not automatically update individual snapshot 
copies in eXe, a fresh copy of the article will need to be taken. Likewise, 
changes made in eXe will not be updated in Wikipedia. </p> <p>Wikipedia content 
is covered by the GNU free documentation license.</p> <p>The reading activity, as the name 
suggests, should ask the learner to perform some form of activity. This activity 
should be directly related to the text the learner has been asked to read. 
Feedback to the activity where appropriate, can provide the learner with some 
reflective guidance.</p> <p>Where you have a number of images that relate 
to each other or to a particular learning exercise you may wish to display 
these in a gallery context rather then individually.</p> ????? A Reference to a resource from which the present resource is derived. A case study is a device that provides learners 
with a simulation that has an educational basis. It takes a situation, generally 
based in reality, and asks learners to demonstrate or describe what action they 
would take to complete a task or resolve a situation. The case study allows 
learners apply their own knowledge and experience to completing the tasks 
assigned. when designing a case study consider the following:<ul> 
<li>	What educational points are conveyed in the story</li>
<li>	What preparation will the learners need to do prior to working on the 
case study</li>
<li>	Where the case study fits into the rest of the course</li>
<li>	How the learners will interact with the materials and each other e.g.
if run in a classroom situation can teams be setup to work on different aspects
of the case and if so how are ideas feed back to the class</li></ul> A date of an event in the lifecycle of the resource. A hint may be provided to assist the 
learner in answering the question. A reference to a related resource. Abkhazian  Actions Activity Add Field Add Image Add JPEG Image Add Page Add Previous/Next links within SCOs? Add a flash video to your iDevice. Add a single text line to an iDevice. Useful if you want the ability to place a label within the iDevice. Add a text entry box to an iDevice. Used for entering larger amounts of textual content. Add all the files provided for the applet
except the .txt file one at a time using the add files and upload buttons. The 
files, once loaded will be displayed beneath the Applet code field. Add an attachment file to your iDevice. Add an interactive feedback field to your iDevice. Add an mp3 file to your iDevice. Add another Option Add another Question Add another activity Add another option Add another question Add files Add images Add the required files to generate a SCORM package editable by eXeLearning Afar  Afrikaans  Albanian  Align: Alignment allows you to 
choose where on the screen the image will be positioned. Alignment allows you to 
choose where on the screen the media player will be positioned. Allow auto completion when 
                                       user filling the gaps. Allow auto completion when user filling the gaps. Although more often used in formal testing 
situations MCQs can be used as a testing tool to stimulate thought and  
discussion on topics students may feel a little reticent in responding to. 

When designing a MCQ test consider the following:
<ul>
<li> What learning outcomes are the questions testing</li>
<li>    What intellectual skills are being tested</li>
<li> What are the language skills of the audience</li>
<li> Gender and cultural issues</li>
<li> Avoid grammar language and question structures that might provide 
     clues</li>
</ul>
  Amharic  An account of the content of the resource. An activity can be defined as a task or set of tasks a learner must
complete. Provide a clear statement of the task and consider any conditions
that may help or hinder the learner in the performance of the task. An entity primarily responsible for making the content of the resource. An entity responsible for making contributions to the content of the resource. An entity responsible for making the resource available. An unambiguous reference to the resource within a given context. Answsers Applet Code: Applet Type Apply Arabic  Armenian  Article Assamese  Attachment Attachment %s has no parentNode Auckland University of Technology Author: Authoring Autoevaluacion Autoevaluación Avestan  Aymara  Azerbaijani  Background image for a header (an image 100px high is recommended). Bad number of arguments supplied Bashkir  Basque  Bengali; Bangla  Bihari  Bislama  Bosnian  Breton  Bulgarian  Burmese  Button Caption Byelorussian; Belarusian  Can NOT Undo Edits Cancel Cannot access directory named  Caption: Case Study Caso pr&aacute;ctico es un iDevice que permite al alumnado introducirse en una historia que le guiar&aacute; a trav&eacute;s de su aprendizaje. Caso práctico Catalan  Catalan Wikipedia Article Chamorro  Change Image Chechen  Check Caps? Checking this box will cause eXe to add Previous and Next links to individual pages within your SCO.  This requires a non-standard extension to SCORM 1.2 and is only known to work with some versions of Moodle. Checking this option the exported SCORM file will include a file named "singlepage_index.html" containing the result of exporting this eXe package as a single page. Checking this option the exported SCORM file will include the result of exporting this eXe package as Web Site. All the html files will have the "website_" prefix to differentiate them from their SCORM equivalent. Checking this option, the exported SCORM file will be editable with eXeLearning. Chichewa; Nyanja  Chinese  Chinese Wikipedia Article Choose an Image Choose an MP3 file Choose an optional image to be shown to the student on completion of this question Choose the size you want 
your image to display at. The measurements are in pixels. Generally, 100 
pixels equals approximately 3cm. Leave both fields blank if you want the 
image to display at its original size. Church Slavic  Chuvash  Citas para pensar Citas para pensar es un iDevice que permite al alumnado reflexionar sobre algunas citas propuestas. Clear Image Clear Recent Projects List Clic on AddFiles button for select the .ggb file and clic on Upload button after. Clic on AddFiles button for select the .jclic.zip file and clic on Upload button after.<p>The activity will be visible when the HTML file will be generated from eXe. Clic on AddFiles button for select the .sb or .scratch file and clic on Upload button after. Click 
on the picture below or the "Add Image" button to select an image file to be 
magnified. Click <strong>Select a file</strong>, browse to the file you want to attach and select it. Click Here Click for completion instructions Click here Click on Preview button to convert 
                                  the latex into an image. Click on the Add images button to select an image file. The image will appear below where you will be able to label it. It's always good practice to put the file size in the label. Close Cloze Cloze Activity Cloze Text Clozelang Contributors: Copy source also in target Cornish  Correccion es un iDevice que permite resaltar texto para hacer correcciones a los autores. Correct Correct Option Correct XLIFF import Correct! Corsican  Couldn't load file, please email file to bugs@exelearning.org Coverage: Create editable SCORM file? Create the case story. A good case is one 
that describes a controversy or sets the scene by describing the characters 
involved and the situation. It should also allow for some action to be taken 
in order to gain resolution of the situation. Creative Commons Attribution 2.5 License Creative Commons Attribution 3.0 License Creative Commons Attribution No Derivatives 3.0 License Creative Commons Attribution Non-commercial 3.0 License Creative Commons Attribution Non-commercial No Derivatives 3.0 License Creative Commons Attribution Non-commercial Share Alike 3.0 License Creative Commons Attribution Share Alike 3.0 License Creative Commons Attribution-NoDerivs 2.5 License Creative Commons Attribution-NonCommercial 2.5 License Creative Commons Attribution-NonCommercial-NoDerivs 2.5 License Creative Commons Attribution-NonCommercial-ShareAlike 2.5 License Creative Commons Attribution-ShareAlike 2.5 License Creator: Croatian  Czech  D Danish  Date: Debes conocer Debes conocer es un iDevice que permite al alumnado ampliar conocimientos, siendo estos obligatorios para su aprendizaje. Default name for level 1 nodes Default name for level 2 nodes Default name for level 3 nodes Delete Delete File Delete Image Delete option Delete question Demote node down in hierarchy Descartes Describe how learners will assess how 
they have done in the exercise. (Rubrics are useful devices for providing 
reflective feedback.) Describe the activity tasks relevant 
to the case story provided. These could be in the form of questions or 
instructions for activity which may lead the learner to resolving a dilemma 
presented.  Describe the prerequisite knowledge learners should have to effectively
complete this learning. Describe the tasks related to the reading learners should undertake. 
This helps demonstrate relevance for learners. Describe the tasks the learners should complete. Description Description: Destacado es un iDevice que permite resaltar texto para llamar la atención del alumnado. Developing Nations 2.0 Disable All Internal Linking Disable Auto-Top Internal Linking Display as: Don't copy source in target Done Dublin Core Dublin Core Metadata Dutch  Dutch Wikipedia Article Dzongkha; Bhutani  ERROR Element.process called directly with %s class ERROR Element.renderEdit called directly with %s class ERROR Element.renderView called directly with %s class ERROR: Block.renderViewContent called directly EXPORT FAILED! EXPORT FAILED!
%s EXPORT FAILED!
Last succesful export is %s. EXTRACT FAILED!
%s Edit Either your idevices/generic.data file or the package you are loading was created with a newer version of eXe.  Please upgrade eXe and try again. Ejercicio resuelto Emphasis Enable All Internal Linking English  English Wikipedia Article Enter 
the text you wish to associate with the file. Enter a hint here. If you
do not want to provide a hint, leave this field blank. Enter a hint here. If you do not want to provide a hint, leave this field blank. Enter a phrase or term you wish to search 
within Wikipedia. Enter a question for learners 
to reflect upon. Enter a title for the gallery Enter an RSS URL for the RSS feed you 
want to attach to your content. Feeds are often identified by a small graphic
 icon (often like this <img src="/images/feed-icon.png" />) or the text "RSS". Clicking on the 
 icon or text label will display an RSS feed right in your browser. You can copy and paste the
URL into this field. Alternately, right clicking on the link or graphic will open a menu box;
click on COPY LINK LOCATION or Copy Shortcut. Back in eXe open the RSS bookmark iDevice and Paste the URL 
into the RSS URL field and click the LOAD button. This will extract the titles from your feed and
display them as links in your content. From here you can edit the bookmarks and add
 instructions or additional learning information. Enter an answer option. Provide 
a range of plausible distractors (usually 3-4) as well as the correct answer. 
Click on the &lt;Add another option&gt; button to add another answer. Enter any feedback you wish to provide 
to the learner. This field may be left blank. if this field is left blank 
default feedback will be provided. Enter any feedback you wish to provide the learner with-in the feedback field. This field can be left blank. Enter instructions for completion here Enter the URL you wish to display
and select the size of the area to display it in. Enter the available choices here. 
You can add options by clicking the "Add Another Option" button. Delete options 
by clicking the red "X" next to the Option. Enter the available choices here. 
You can add options by clicking the "Add another option" button. Delete options by 
clicking the red X next to the option. Enter the details of the reading including reference details. The 
referencing style used will depend on the preference of your faculty or 
department. Enter the flash display 
dimensions (in pixels) and determine the alignment of the image on screen. 
The width and height dimensions will alter proportionally. Enter the instructions for completion here Enter the label here Enter the question stem. 
The quest should be clear and unambiguous. Avoid negative premises 
as these can tend to be ambiguous. Enter the question stem. 
The question should be clear and unambiguous. Avoid negative premises as these 
can tend to confuse learners. Enter the text you wish to 
                                                associate with the image. Enter the text you wish to 
                                 associate with the image. Enter the text you wish to 
associate with the file. Enter the text you wish to associate with the downloaded file. You might want to provide instructions on what you require the learner to do once the file is downloaded or how the material should be used. Error importing XLIFF: %s Esperanto  Estonian  Euskara Wikipedia Article Example Example Example Export Export <input_file> elp package to optional <output_file> on one of the given formats: xml, scorm, ims, website, webzip , singlepage or xliff. Export format not implemented Export iDevice Exported to %s External Web Site Extra large Extract Package FPD - Actividad de Eleccion Multiple FPD - Actividad de Espacios en Blanco FPD - Actividad de Espacios en Blanco (Modificada) FPD - Actividad de Seleccion Multiple FPD - Actividad de Verdadero/Falso FPD - Caso Practico FPD - Citas Para Pensar FPD - Correccion FPD - Debes Conocer FPD - Destacado FPD - Ejercicio Resuelto FPD - Orientaciones Alumnado FPD - Orientaciones Tutoria FPD - Para Saber Mas FPD - Recomendacion FPD - Reflexiona (con Retroalimentacion) FPD - Reflexiona (sin Retroalimentacion) FPD - Texto Libre False Faroese  Feedback Feedback button will not appear if no 
data is entered into this field. Feedback: Fijian; Fiji  File %s does not exist or is not readable. File '%s' successfully imported to '%s'. Filename %s is a file, cannot replace it Filename: Finnish  Flash Movie Flash with Text Folder name %s already exists. Please choose another one or delete existing one then try again. Footer: Force overwrite of <output_file> Format: Frame Height: Free Text French  French Wikipedia Article Frisian  GNU Free Documentation License Gallegan; Galician  Geogebra Georgian  German  German Wikipedia Article Get score Go Back Go Forward Go Forward (Not Available) Greek  Greek Wikipedia Article Guarani  Gujarati  Hausa (?)  Header Background: Hebrew (formerly iw)  Herero  Hide Hide Feedback Hide/Show Word Hindi  Hint Hiri Motu  Home Hungarian  IDevice Icon IDevice Question Icon IDevice broken IEEE LOM IMS Content Package 1.1.3 Icelandic  Icons Identifier: Ido  If you choose this option, the import process will take the texts from source language instead of target language. If you don't choose this option, target field will be empty. Some Computer Aided Translation tools (i.g. OmegaT ) just translate the content of the target field. If you are using this kind of tools, you will need to pre-fill target field with a copy of the source field. Image Image Gallery Image Magnifier Image with Text Import format not implemented Import from source language Import iDevice Import to <output_file> elp package, <input_file> in one of the given formats: xml or xliff. Include Single Page export file Include Single Page export file? Include Web Site export files Include Web Site export files? Incorrect Incorrect! Indonesian (formerly in)  Info Information Information about rights held in and over the resource. Initial Zoom Insert Package Instant Marking? Instructions Instructions For Learners Interlingua  Interlingue  Internal Linking (for Web Site Exports only) Introduce el texto que aparecer&aacute; en este iDevice Inuktitut  Inupiak  Invalid input package Invalid output package '%s' Irish  It uses material from the  Italian  Italian Wikipedia Article JClic Japanese  Japanese Wikipedia Article Java Applet Javanese  Jose Ramon Jimenez Reyes Kalaallisut; Greenlandic  Kannada  Kashmiri  Kazakh  Khmer; Cambodian  Kikuyu  Kinyarwanda  Kirghiz  Komi  Korean  Kuanyama  Kurdish  Label: Language: Lao; Laotian  Large Latin  Latvian; Lettish  Left Letzeburgesch  Level 1: Level 2: Level 3: License: Licensed under the Lingala  Lithuanian  Load Load Image Local file %s is not found, cannot preview it MP3 Macedonian  Magyar Wikipedia Article Malagasy  Malay  Malayalam  Maltese  Manx  Maori  Marathi  Marshall  Maths Maximum zoom Medium Metadata MimeTeX compile failed!
%s Moldavian  Mongolian  Mostrar Puntuaci&oacute;n? Move Down Move Image Left Move Image Right Move Up Move node down Move node up Multi-choice Multi-select N Name Nauru  Navajo  Ndebele, North  Ndebele, South  Ndonga  Nepali  New iDevice Next No --export or --import option supplied. No Images Loaded No Thumbnail Available. Could not load original image. No Thumbnail Available. Could not shrink image. No Thumbnail Available. Could not shrink original image. No emphasis No file input supplied. None Northern Sami  Norwegian  Norwegian Bokmål  Norwegian Nynorsk  O OK Objectives Objectives describe the expected outcomes of the learning and should
define what the learners will be able to do when they have completed the
learning tasks. Occitan; Provençal  Once you have chosen SCORM export option using '-x scorm' or '--export scorm', it's possible to configure the following export options: Once you have chosen XLIFF export option using '-x xliff' or '--export xliff', it's possible to configure the following export options: Once you have chosen XLIFF import option using  '-i xliff' or '--import xliff', it's possible to configure the following import options: Only select .flv (Flash Video Files) for 
this iDevice. Only select .swf (Flash Objects) for 
this iDevice. Option Options Options --export and --import are mutually exclusive. Orientaciones alumnado es un iDevice que permite al profesorado conocer los objetivos del aprendizje del alumnado. Orientaciones para el alumnado Orientaciones para la tutoría Orientciones alumnado es un iDevice que permite al alumnado conocer los objetivos de su aprendizaje. Oriya  Ossetian; Ossetic  Other Outline Own site P Package Package Properties Package extracted to: %s Package is old. Please upgrade it (using File..Open followed by File..Save As) before attempting to insert it into another package! Package saved to: %s Pali  Panjabi; Punjabi  Para saber m&aacute;s es un iDevice que permite al alumnado ampliar conocimientos, siendo estos voluntarios para su aprendizaje. Para saber m&aacute;s es un iDevice que permite al profesarado recomendar libros, películas, ... útiles para su formación. Para saber más Pashto, Pushto  Pedagogical Help Pedagogical Tip Persian  Please enter an idevice name. Please enter<br />an idevice name. Please select a .flv file. Please select a .jpg file. Please select a correct answer for each question. Please type or paste a valid URL. Please upload a .ggb file. Please upload a .jclic.zip file. Please upload a .sb or .scratch file. Please use eXe's
   File... Quit
menu to close eXe. Please wait until loading finishes Please write: scene number,URL (no spaces) that include it, eg: 3,http://example.com; clic on Upload button after. Polish  Polish Wikipedia Article Portugese Wikipedia Article Portuguese  Preferences Preknowledge Prerequisite knowledge refers to the knowledge learners should already
have in order to be able to effectively complete the learning. Examples of
pre-knowledge can be: <ul>
<li>        Learners must have level 4 English </li>
<li>        Learners must be able to assemble standard power tools </li></ul>
 Preview Preview directory %s is a file, cannot replace it Previous Primary author of the resource. Project Properties Project Title: Promote node up in hierarchy Properties Provide a caption for the 
MP3 file. This will appear in the players title bar as well. Provide a caption for the 
image to be magnified. Provide a caption for the flash movie 
you have just inserted. Provide a caption for the flash you 
                                  have just inserted. Provide a caption for the image 
you have just inserted. Provide instruction on how the True/False Question should be 
completed. Provide instruction on how the cloze activity should be 
completed. Default text will be entered if there are no changes to this field.
 Provide relevant feedback on the 
situation. Publisher: Purpose Put instructions for learners here Q Quechua  Question Question: Quit R RSS Read the paragraph below and fill in the missing words Read the paragraph below and fill in the missing words. Reading Activity Reading Activity 0.11 Recomendación Reflection Reflection is a teaching method often used to 
connect theory to practice. Reflection tasks often provide learners with an 
opportunity to observe and reflect on their observations before presenting 
these as a piece of academic work. Journals, diaries, profiles and portfolios 
are useful tools for collecting observation data. Rubrics and guides can be 
effective feedback tools. Reflective question: Reflexiona Relation: Rename Restart Retroalimentacion Rhaeto-Romance  Right Rights: Romanian  Rundi; Kirundi  Russian  S SAVE FAILED! SAVE FAILED!
%s SAVE FAILED!
Last succesful save is %s. SCORM 1.2 SCORM 1.2 Options SCORM Quiz SCORM export options SUBMIT ANSWERS Samoan  Sango; Sangro  Sanskrit  Sardinian  Save Scots; Gaelic  Scratch Section Select Flash Object Select Format Select Language Select License Select MP3 file Select a Format. Select a file Select a flash video Select a font size:  Select a language. Select a license. Select an MP3 Select an image Select an image (JPG file) Select as many correct answer 
options as required by clicking the check box beside the option. Select pass rate:  Select source language Select target language Select the appropriate language version 
of Wikipedia to search and enter search term. Select the correct option by clicking 
on the radio button. Select the size of the magnifying glass Self-contained Folder Serbian  Sesotho; Sotho, Southern  Set the initial level of zoom 
when the IDevice loads, as a percentage of the original image size Set the maximum level of zoom, 
as a percentage of the original image size Settings saved Shona  Show %s Image Show Answers Show Feedback Show Image Show/Clear Answers Show/Hide Feedback Sindhi  Sinhalese  Site Size of magnifying glass:  Slovak  Slovenian  Slovenian Wikipedia Article Small Solucion Somali  Some emphasis Sorry, this URL is unreachable Sorry, wrong file format Sorry, wrong file format. Sorry, wrong file format:
%s Source: Spanish  Spanish Wikipedia Article Story: Strict Marking? Subject: Submit Submit Answer Successfully exported '%s' from '%s'. Sundanese  Swahili  Swati; Siswati  Swedish  Swedish Wikipedia Article Tagalog  Tahitian  Tajik  Tamil  Tatar  Taxonomy Teachers should keep the following in mind when using this iDevice: <ol><li>Think about the number of different types of activity planned for your resource that will be visually signalled in the content. Avoid using too many different types or classification of activities otherwise learner may become confused. Usually three or four different types are more than adequate for a teaching resource.</li><li>From a visual design perspective, avoid having two iDevices immediately following each other without any text in between. If this is required, rather collapse two questions or events into one iDevice. </li><li>Think about activities where the perceived benefit of doing the activity outweighs the time and effort it will take to complete the activity. </li></ol> Telugu  Text Text Box Text Line Texto Caso pr&aacute;ctico: Texto Citas para pensar: Texto Correci&oacute;n: Texto Debes conocer: Texto Destacado: Texto Orientaciones alumnado Texto Orientaciones alumnado: Texto Orientaciones tutor&iacute;a Texto Orientaciones tutor&iacute;a: Texto Para sabe m&aacute;s Texto Para saber m&aacute;s Texto de la recomendaci&oacute;n: Texto para pensar: Thai  The MP3 iDevice allows you to attach an MP3 media file to your content along with relevant textuallearning instructions. The RSS iDevice is used 
to provide new content to an individual users machine. Using this
iDevice you can provide links from a feed you select for learners to view. The attachment iDevice is used to attach existing files to your .elp content. For example, you might have a PDF file or a PPT presentation file that you wish the learners to have access to, these can be attached and labeled to indicate what the attachment is and how large the file is. Learners can click on the attachment link and can download the attachment. The extent or scope of the content of the resource. The external website iDevice loads an external website 
into an inline frame in your eXe content rather then opening it in a popup box. 
This means learners are not having to juggle windows. 
This iDevice should only be used if your content 
will be viewed by learners online. The flash with text idevice allows you to 
associate additional textual information to a flash file. This may be useful 
where you wish to provide educational instruction regarding the flash file 
the learners will view. The image magnifier is a magnifying tool enabling
 learners to magnify the view of the image they have been given. Moving the 
magnifying glass over the image allows larger detail to be studied. The majority of a learning resource will be 
establishing context, delivering instructions and providing general information.
This provides the framework within which the learning activities are built and 
delivered. The mathematical language LATEX has been 
                        used to enable your to insert mathematical formula 
                        into your content. It does this by translating 
                        LATEX into an image which is then displayed
                         within your eXe content. We would recommend that 
                        you use the Free Text iDevice to provide 
                        explanatory notes and learning instruction around 
                        this graphic. The name given to the resource. The nature or genre of the content of the resource. The project's title. The purpose dialogue allows you to describe your intended purpose of the iDevice to other potential users. The topic of the content of the resource. This article is licensed under the  This chooses the initial size 
of the magnifying glass This iDevice only supports the Flash Video File (.FLV) format, and will not
accept other video formats. You can however convert other movie formats
(e.g. mov, wmf etc) into the .FLV format using third party encoders. These
are not supplied with eXe. Users will also need to download the Flash 8
player from http://www.macromedia.com/ to play the video. This is a free text field general learning content can be entered. This is a free text field. This is an example of a user created
iDevice plugin. This is an optional field. This option will wrap all the exported fields in CDATA sections. This kind of sections are not recommended by XLIFF standard but it could be a good option if you want to use a pre-process tool (i.g.: Rainbow) before using the Computer Aided Translation software. This will delete this iDevice.
Do you really want to do this? Tibetan  Tigrinya  Tile background image? Tip: Title Title: To indicate the correct answer, 
click the radio button next to the correct option. Tongan   Topic True True-False Question True/false questions present a statement where 
the learner must decide if the statement is true. This type of question works 
well for factual information and information that lends itself to either/or 
responses. Tsonga  Tswana; Setswana  Turkish  Turkmen  Twi  Type a discussion topic here. Type in the feedback that you want the 
student to see when selecting the particular option. If you don't complete this 
box, eXe will automatically provide default feedback as follows: "Correct 
answer" as indicated by the selection for the correct answer; or "Wrong answer"
for the other options. Type in the feedback that you want the 
student to see when selecting the particular question. If you don't complete
this box, eXe will automatically provide default feedback as follows: 
"Correct answer" as indicated by the selection for the correct answer; or 
"Wrong answer" for the other alternatives. Type in the feedback you want 
to provide the learner with. Type the learning objectives for this resource. Type the question stem. The question 
should be clear and unambiguous. Avoid negative premises as these can tend to 
be ambiguous. Type: Typically, a Rights element will contain a rights management statement for the resource, or reference a service providing such information. Rights information often encompasses Intellectual Property Rights (IPR), Copyright, and various Property Rights. If the Rights element is absent, no assumptions can be made about the status of these and other rights with respect to the resource. U URL: Uighur  Ukrainian  Unable to download from %s <br/>Please check the spelling and connection and try again. Unable to load RSS feed from %s <br/>Please check the spelling and connection and try again. Unable to merge: duplicate Java Applet resource names exist (including: " Undo Edits Unit University of Auckland Unlike the MCQ the SCORM quiz is used to test 
the learners knowledge on a topic without providing the learner with feedback 
to the correct answer. The quiz will often be given once the learner has had 
time to learn and practice using the information or skill.
  Update Tree Upload Urdu  Usage: %prog [options] input_file [output_file]

To show help:
%prog -h Use feedback to provide a summary of the points covered in the reading, 
or as a starting point for further analysis of the reading by posing a question 
or providing a statement to begin a debate. Use this Idevice if you have a lot of images to show. Use this field to describe your intended use and the pedagogy behind the device's development. This can be useful if your iDevice is to be exported for others to use. Use this field to enter text. This 
iDevice has no emphasis applied although limited formatting can be applied to 
text through the text editing buttons associated with the field. Use this pulldown to select whether or not  the iDevice should have any formatting  applied to it to distinguish it; ie. a border and an icon. Uzbek  VK_DELETE VK_INSERT VK_LEFT VK_RIGHT Vietnamese  Volapük; Volapuk  Walloon  Web Site Web pages footer. Welsh  What to read When building an MCQ consider the following: <ul>
<li> Use phrases that learners are familiar with and have 
encountered in their study </li>
<li> Keep responses concise </li>
<li> There should be some consistency between the stem and the responses </li>
<li> Provide enough options to challenge learners to think about their response
</li>
<li> Try to make sure that correct responses are not more detailed than the 
distractors </li>
<li> Distractors should be incorrect but plausible </li>
</ul>
 Wiki Article Wikibooks Article Wikieducator Content Wikiversity Wiktionary Wolof  Wrap fields in CDATA Wrong XHTML XLIFF export options XLIFF import options Xhosa  Yiddish (formerly ji)  Yoruba  You can use the toolbar or enter latex manually into the textarea.  Your new iDevice will appear in the iDevice pane with this title. This is a compulsory field and you will be prompted to enter a label if you try to submit your iDevice without one. Zhuang  Zip File Zulu  article  blank for original size debugInfo eXe eXe : elearning XHTML editor eXe Project eXeex - About exe_do: error: Unable to export from '%s'.
The error was: exe_do: error: Unable to import '%s'.
The error was: exelearning file %s has no parentNode http://en.wikipedia.org/ iDevice %s has no package iDevice Editor iDevice icon iDevicePane iDevices label="About eXe" accesskey="a" label="Common Cartridge" accesskey="c" label="Export" accesskey="e" label="Extract Package" accesskey="E" label="File" accesskey="f" label="HTML Course" accesskey="h" label="Help" accesskey="h" label="IMS Content Package" accesskey="i" label="Import" accesskey="i" label="Insert Package" accesskey="i" label="Merging" accesskey="m" label="New" accesskey="n" label="Open" accesskey="o" label="Print" accesskey="p" label="Quit" accesskey="q" label="Recent Projects..." accesskey="r" label="Refresh Display" accesskey="r" label="Release Notes" accesskey="n" label="Report an Issue" accesskey="r" label="SCORM 1.2" accesskey="s" label="Save As..." accesskey="a" label="Save" accesskey="s" label="Single Page" accesskey="p" label="Styles" accesskey="s" label="Text File" accesskey="t" label="Tools" accesskey="t" label="XLIFF file" accesskey="x" label="XLIFF" accesskey="x" label="eXe Live Chat" accesskey="c" label="eXe Manual" accesskey="m" label="eXe Tutorial" accesskey="u" label="eXe Web Site" accesskey="w" label="iPod Notes" accesskey="n" newline outlinePane renamed a Resource for an Applet Idevice, and it should not have even made it this far! show this help message and exit text Project-Id-Version: eXe trunk (1.04.1)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-07-26 10:49+0200
PO-Revision-Date: 2012-07-03 10:22+0200
Last-Translator: José Miguel Andonegi <jmandonegi@ulhi.net>
Language-Team: eXe <exelearning.euskaraz@gmail.com>
Language: Basque
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
 
                 Data, normalean, baliabidearen sorrerarekin edo
                 erabilgarritasunarekin lotuta egoten da. Data kodetzeko
                 modurik egokiena, ondoko izena duen profilean zehaztuta dago
                  
 eta, besteak beste, UUUU-HH-EE moduan emaniko datak ditu.
             
                Gaia, normalean, gakoen, esaldien edo sailkapen kodeen bidez 
                azalduko da. Egokiena, kontrolatutako hiztegi edo sailkapen eskema formal 
                batetik balioak aukeratzea da.
              
Baliabidearen itxura fisikoa edo digitala deskribatzeko, 
erabili FORMAT elementua.
             
              Motaren barruan, eduki bakoitzeko kategoria orokorrak, funtzioak eta/edo generoak
              deskribatzen dituzten terminoak datoz. Egokiena hiztegi kontrolatu batetik edo sailkapen
              eskema kontrolatu batetik balio bat aukeratzea da.   
                  <p>Gehitzen ari zaren appleta zerrenda honetako programa 
batekin sortuta badago, mesedez, aukeratu, eta ondoren gehitu 
zure programak sortutako datua/applet artxiboa. <p>Gehitzen ari zaren appleta zerrenda honetako programa 
batekin sortuta badago, mesedez, aukeratu, eta ondoren gehitu 
zure programak sortutako datua/applet artxiboa. </p>
<p>Adib. Geogebra applet-en kasuan, aukeratu geogebra eta ondoren 
gehitu Geogebran sortu duzun .ggb artxiboa.</p> "%s" existitzen da dagoeneko.
Mesedez, saiatu beste izen batekin. Bilatu ".txt artxiboa (applet artxiboan) 
eta ireki. Kopiatu artxiboaren edukia <ctrl A, ctrl C> appletaren kode
eremuan. (Afan) Oromoera ---Ondokora mugitu--- . Mesedez, ASCII izenak erabili. <dl>  <dt>Zure asmoa oinarrizko kontzeptuak edo irakurgaia zenbateraino ulertu dituzten frogatzea bada </dt>  <dd>    <p>  Idatzi kontzeptuaren edo irakurgaiaren laburpena, ondo ulertu ote den frogatzeko beste besteko luzeraz, baina luzeegia izan gabe, ez dadin nekagarria izan. Aski da orrialde bat baino gutxiago idaztea. Ikasle gazteentzat edo hasiberrientzat, gainera, hobe are laburragoa izatea.    </p>    <p>Testuan, nabarmendu kontzeptuak ulertzeko funtsezkoak diren hitzak; bereziki, aditzak, izenak eta aditzondoak. Hautatu erantzun argi bakarra duten alternatibak.    </p>  </dd>  <dt>Zure asmoa hiztegia zenbateraino ulertu duten frogatzea bada  </dt>  <dd><p>Idatzi hiztegiko terminoak jasotzen dituen testu bat. Zaindu testuaren koherentzia, kohesioa eta luzera. Nabarmendu testuko funtsezko hitzak. Hautatu erantzun argi bakarra duten alternatibak.</p>  </dd>  <dt>Zure asmoa hitzen konposaketa/gramatika frogatzea bada:  </dt>  <dd>  <p>Idatzi funtsezko konposaketak dituen testu bat. Zaindu testuaren koherentzia, kohesioa eta luzera. Gogoan izan helburua ez dela hiztegia ezagutzea; beraz, ikasleentzat aski ezaguna beharko du izan hitzen esanahiak.  </p>  <p>Nabarmendu testuko funtsezko hitzak. Proposatu erro bera baina esanahi ezberdina duten alternatibak. Egokia litzateke testua/ariketa lankide bati irakurtzeko ematea, sorpresarik egon ez dadin!  </p> <ol>  <li>Klikatu &lt;Aukeratu MP3 bat&gt; eta bilatu gehitu nahi duzun artxiboa </li> <li>Klikatu zerrendan eta aukeratu artxiboa non erakutsi nahi duzun.</li><li>Nahi izanez gero, idatzi artxiboaren izenburua.</li> <li>Lotu testu bat MP3 artxiboari.</li> <li>Aukeratu iDevice-a zer estilotan agertuko den. 'Enfasi pixka bat' aukerak, adibidez, ertza eta ikonoa jartzen dizkio edukiari.</li></ol> <p>
iDevice testua duen irudia eXe edukiaren dimentsio emozionalerako 
(afektiboa) eta ikasteko jarduerarako (kognitiboa) erabil daiteke.
</p><p>
<b>Irudiak eta testu laburpenak integratzea</b>
</p><p>
Psikologo kognitiboen arabera, ikasleei irudi adierazgarri bat
eta laburpena ahoz aldi berean emateak murriztu egiten du 
karga kognitiboa, eta ikasketa areagotzen. iDevice hau irudi bat
(argazkia, diagrama edo grafikoa) eta irudiaren puntu nagusien laburpena
aurkezteko erabil daite. Adibidez, lau denborako motor baten funtzioak
azaltzen ari bazara, pistoi bakoitzaren kokapena erakuts dezakezu, eta horien
gaineko azalpen laburra eman.</p> <p>Esleitu etiketa bat atxikitako artxiboari. Lagungarria da zer artxibo mota den adierazteko (pdf, ppt, etab.)</p><p>Komenigarria da neurria zehaztea ere, webgune batera esportatzean ikasleek deskargatzeko zenbat denbora beharko duten jakin dezaten.</p><p>Adibidez:</p><code>Salmenten Forecast.doc (500kb)</code></p> <p>Hutsuneak betetzeko ariketetan, ikasleek testuan falta diren hitzak edo esaldiak idatzi beharko dituzte. Ariketa horiek hainbat helburu izaten dituzte:</p><ol><li>Ikastaroko oinarrizko kontzeptuak ezagutzen ote diren egiaztatzea (aurrekontrol, trebakuntza ariketa edo froga gehigarri gisa).</li><li>Irakurgaia zenbateraino ulertu den frogatzea.</li><li>Hiztegia zenbateraino ezagutzen den egiaztatzeko.</li><li>Hitz osaketa/gramatika ziurtatzeko. </li></ol> <p>Sartu eremuan hutsuneak betetzeko testua. Horretarako,
kopiatu beste iturri batetik edo zuzenean bertan idatzi.</p><p>Ezkutatu 
beharreko hitzak aukeratzeko, klikatu bi aldiz hitzaren gainean
eta klikatu beheko Ezkutatu/Erakutsi hitza botoia.</p> <p>Egiaztatu gabe utziz gero, idazketa errore gutxi batzuk onartuko dira. Egiaztatuta badago, idazketako edo maiuskula errore bakar bat onartuko da.</p><p><strong>Adibidez:</strong> Erantzun zuzena <code>Elefantea</code> bada, <code>elefantea</code> edo <code>Elifantea</code> <em>"oso antzekoak"</em> izango dira algoritmoarentzat, letra oker bakarra baitute, nahiz eta "Maiuskulak egiaztatu" gaituta egon.</p><p>Itzalita badago, goiko adibideko <code>e</code> minuskula ez da erroretzat joko eta <code>elifantea</code> ere onartu egingo da.</p><p>"Zorroztasunez zuzendu" eta "Maiuskulak egiaztatu" gaituta badaude, erantzun zuzen bakarra "Elefantea" izango da. "Zorroztasunez zuzendu" soilik badago gaituta, "elefantea" ere onartuko da.</p>" <p>Aukera hau gaituta badago, maiuskulak eta minuskulak gaizki jarrita dituzten erantzunak okerrak izango dira.</p> <p>Aukera hau gaituta badago, hitzak ikasleak idazten dituen bitartean
markatuko dira, eta ez ariketaren amaieran.</p> <p>Formula matematikoa sortzeko, aukeratu sinboloak beheko testu editoretik edota sartu LATEXa eskuz. LATEXa nola agertuko den aurreikusteko, erabili beheko &lt;Aurrebista&gt; botoia.</p> <p>Aukera hau markatuta badago, lortutako puntu-kopurua erakutsiko da.</p> Irakasleek ohar hauek gogoan izan beharko lituzkete iDevice hau erabiltzean: <ol><li>Pentsatu zenbat ekintza mota erabili nahi dituzuen bisualki edukian. Ez erabili jarduera mota edo sailkapen gehiegi, ikasleentzat nahasgarria izan baitaiteke. Askotan, nahikoak dira hiruzpalau mota.</li><li>Diseinu bisualaren ikuspuntutik, saiatu bi iDevice elkarren ondoan ez jartzen, tartean testurik ez dutela. Beharrezkoa izanez gero, iDevice bakarrean sartu bi galdera edo ekintza.</li><li>Prestatzen dituzun jarduerei esker ikasleek lortutako onurak handiagoa izan behar du ekintza gauzatzeko emandako denborak eta ahaleginak baino. </li></ol> <p>Irakurtzeko ariketa, hasiera batean, 
ikasleak testua ulertzeko duen gaitasuna egiaztatzeko erabiliko da. Horretarako, 
eskatu ikasleari testuaren inguruan hausnartzeko eta galderak erantzuteko, edo 
jarduera fisikoagoa jarri.</p> <p>Wikipediaren iDevice-ak Wikipediaren edukia
bilatzeko eta eXe-n deskargatzeko aukera ematen dizu. Wikipedia Artikuluaren iDevice-ak
artikuluaren edukiaren kopia egiten du. Wikipediako aldaketak ez dira automatikoki
eguneratzen eXe-ko kopietan; beste kopia bat egin behar zaie. Eta
eXe-n egindako aldaketak ez dira Wikipedian eguneratuko. </p> <p>Wikipediaren edukia
GNUren dokumentazio librearen lizentziak babestuta dago.</p> <p>Irakurketa ariketak, izenak dioen bezala,
jarduera bat egiteko eskatu beharko lioke ikasleari. Ariketak ikasleak irakurri 
duen testuarekin lotuta egon beharko luke. Ariketari lotutako feedbackek
hausnartzeko gidak eman ditzakete.</p> <p>Posible da elkarren artean edota ariketa batekin lotutako
irudi batzuk egotea eta, banaka beharrean galerian erakustea 
nahiago izatea.</p> ????? Baliabide honen jatorria den baliabidearen aipamena. Ikasketa kasua ikasleentzako tresna bat da, 
hezteko asmoa duten simulazioak ematen dituena. Gehienetan errealitatean 
oinarrituta egoten den egoera batetik abiatuta, ikasleei arazoa konpontzeko zer egingo luketen 
adierazteko eskatzen zaie. Hala, beren ezagutzak eta esperientzia 
aplikatu beharko dituzte ataza betetzeko. Ikasketa kasua diseinatzeko orduan, 
ondokoak izan behar ditu kontuan:<ul> 
<li>	Istorioak dituen asmo hezitzaileak</li>
<li>	Ikasketa kasuan lanean hasi aurretik, ikasleek behar duten prestakuntza </li>
<li>	Ikasketa kasua ikastaroko zein unetan eman</li>
<li>	Ikasleek materialak nola erabiliko dituzten. 
Adibidez, ikasgelan hainbat talde sor daitezke, bakoitzak alderdi jakin bat jorra dezan;
hala, eztabaidagai gehiago agertuko dira.</li></ul> Baliabidearen bizitza zikloaren gartaera baten data. Arrasto bat eman diezaiokezu ikasleari, 
galdera erantzuten laguntzeko. Antzeko baliabide baten aipamena. Abjasiera Ekintzak Jarduera Gehitu eremua Gehitu irudia Gehitu JPEG irudia Gehitu orria Aurreko/Hurrengo estekak SCOs-en gehitu? Gehitu flash bideo bat zure iDevice-ri. Gehitu testu lerro bat iDevice batera. Erabilgarria da iDevice-ren barruan etiketa bat jartzeko. Gehitu testu koadroa iDevice batera. Testu luzeagoak gehitzeko erabiltzen da. Appleterako emandako artxibo guztiak 
( .txt artxiboa izan ezik) aldi berean gehitzeko, erabili gehitzeko eta kargatzeko botoiak.
Artxiboak kargatuta daudenean, azpiprogramaren kode eremuaren azpian agertuko dira. Gehitu atxikimendu bat zure iDevice-ri. Gehitu feedback interaktibo bat zure iDevice-ri. Gehitu MP3 artxiboa iDevice-an. Gehitu beste aukera Gehitu beste galdera bat Gehitu beste jarduera bat Gehitu beste aukera Gehitu beste galdera bat Gehitu artxiboak Gehitu irudiak Sortutako SCORM paketea eXeLearningek editatu ahal izateko beharrezkoak diren fitxategiak gehitu Urrun Afrikaansa Albaniera Lerrokatu: Lerrokatzean, irudia 
pantailan non kokatu nahi duzun zehaztuko duzu. Lerrokatzean, media player 
pantailan non kokatu nahi duzun aukeratuko duzu. Erabiltzailea hutsuneak betetzen ari denean, 
automatikoki betetzeko baimena. Erabiltzailea hutsuneak betetzen ari denean, automatokiki osatzeko baimena. Askotan froga formaletan erabiltzen diren arren, 
MCQak pentsamendua eta eztabaida estimulatzeko tresna gisa 
balia daitezke ikasleei erantzuteko zailak egiten zaizkien gaietan. 

MCQ froga bat diseinatzeko, ondokoak izan behar ditu kontuan:<ul>
<li>Zer eduki landuko diren galderen bidez</li>
<li>Zer gaitasun intelektual ebaluatuko diren</li>
<li>Ikasleen hizkuntza gaitasunak </li>
<li>Genero eta kultur gaiak </li>
<li>Galderaren gramatika eta egitura zaintzea, arrastorik ez emateko </li>
</ul> Amharera Baliabidearen edukiaren azalpena. Jarduera bat ikasleek bete behar duten ataza batez edo
atazen multzo batez osatuta egon daiteke. Atazaren azalpen garbia eman,
eta ikasleak ataza gauzatzeko edo ez betetzeko baldintzak, kontuan izan. Baliabidearen edukia sorzteko ardura duen entitatea. Baliabidearen edukiari ekarpenak egiteko ardura duen entitatea. Baliabidearen edukia eskuragarria egiteko ardura duen entitatea. Testuinguru jakin baten barnean anbiguoa ez den baliabide erreferentzia. Erantzunak Applet kodea: Applet mota: Aplikatu Arabiera Armeniera Artikulua Assamera Atxikitako artxiboa %s atxikitako artxiboak ez du ahaide nodorik Auckland University of Technology Egilea: Edizioa Autoebaluazioa Autoebaluazioa Avestera Aimara Azerbaijanera Goiburuaren atzeko irudia (gomendatzen da 100px-etako altuera duen irudia). Jasotako argumentu kopurua ez da egokia Bashkirrera Euskara Bengalera Bihariera Bislama Bosniera Bretoiera  Bulgariera Birmaniera Epigrafe botoia Bielorrusiera Ezin da desegin editatutakoa Ezeztatu Ezin da izendatutako direktoriora sartu Epigrafea: Kasu azterketa Kasu praktikoa iDevicearen bidez, ikaslea bere ikasketa prozesuaren zehar gidatzeko balio duen istorio bat kontatu dezakezu. Kasu praktikoa Katalana Wikipediako artikulua katalanez Txamorroa Aldatu irudia Txetxeniera Maiuskulak egiaztatu? Koadrotxo hau klikatuz gero, eXek Hurrengoa eta Aurrekoa estekak gehituko ditu zure SCOaren orri bakoitzean. Honek, SCORMaren luzapen ez-estandar bat eskatzen du eta bakarrik funtzuinatuko du Moodle-en bertsio batzutan. Aukera hau markatzen baduzu, "singlepage_index.html" izeneko fitxategi bat sortuko da SCORM fitxategiaren barnean, eXe paketearen orri bakarreko esportazioarekin. Aukera hau markatzen baduzu, eXe paketearen webgune esportazioa sartuko da SCORM fitxategian. Fitxategi guztiei "website_" aurrizkia jarriko zaie SCORM-erako erabilitakoa bereizteko. Aukera hau markatzen baduzu, sortutako SCORM fitxategia eXeLearning-ekin editatu ahal izango duzu. Txitxeuera; Nyanja Txinera Wikipediako artikulua txineraz Aukeratu irudia Aukeratu MP3 artxibo bat Aukeratu aukerazko irudi bat, ikasleari galdera hau osatzen duenean erakusteko Aukeratu zer neurritan azalduko den 
zure irudia. Neurriak pixeletan daude. Oro har, 100 
pixelek 3 cm inguru dituzte. Irudia jatorrizko neurrian 
erakutsi nahi baduzu, utzi eremuak bete gabe. Eslaviar eliza Txubaxiera Hausnarketarako aipua Hausnarketarako aipua idevicearen bidez, ikaslearen hausnarketa sortarazi dezaketen aipuak proposatu ditzakezu. Ezabatu irudia Garbitu azken proiektuen zerrenda Klikatu 'Fitxategiak gehitu' botoia ggb fitxategia aukeratzeko eta klikatu 'Igo' botoia. Klikatu 'fitxategiak gehitu' botoia .jclic .zip fitxategia aukeratzeko eta klikatu 'Igo' botoia. <p> Aktibitatea ikusi ahal izango duzu eXe-kin HTML fitxategia sortzen duzunean. klikatu 'Fitxategiak gehitu' botoia .sb edo .scratch fitxategia aukeratzeko eta klikatu 'Igo' botoia. <p> Aktibitatea ikusi ahal izango duzu eXe-kin HTML fitxategia sortzen duzunean. Klikatu 
beheko irudia edo "Gehitu irudia" botoia, irudi bat handitzeko. Klikatu <strong>Aukeratu artxibo bat</strong>, bilatu atxiki nahi duzun artxiboa, eta aukeratu. Klikatu hemen Klikatu argibideak osatzeko Klikatu hemen Kiklatu Aurrebista botoia 
                                  latex-a irudi bihurtzeko. Klikatu irudiak gehitzeko botoia irudi artxibo bat aukeratzeko. Irudia beheko aldean agertuko da eta etiketak jarri ahalko dizkiozu. Lagungarria da etiketan artxiboaren neurria adieraztea. Itxi Bete hutsuneak Hutsuneak betetzeko jarduera Hutsuneak betetzeko testua Bete hutsuneak Kolaboratzaileak: Jatorrizko testua xedean kopiatu Kornubiera Zuzenketa, iDevicearen bidez, testuak nabarmendu ditzakezu egileei zuzenketak egiteko. Aukera zuzena. Zuzena. XLIFF inportazio zuzena Zuzena. Korsikera Ezin da artxiboa kargatu. Mesedez, bidali artxiboa bugs@exelearning.org helbidera Estaldura: SCORM fitxategi editagarria sortu? Sortu kasuaren istorioa. Kasuak, ona izateko, 
gatazka bat deskribatu beharko luke, edo pertsonaien bidez egoera azaldu. 
Gainera, gatazka konpondu ahal izateko 
hainbat ekintza gauzatzeko aukera eman beharko luke. Creative Commons Attribution 2.5 lizentzia Creative Commons Attribution 3.0 lizentzia Creative Commons Attribution-Derivatives 3.0 lizentzia Creative Commons Attribution Non-commercial 3-0 lizentzia Creative Commons Attribution Non-commercial No Derivatives 3.0 lizentzia Creative Commons Attribution Non-commercial Share Alike 3.0 lizentzia Creative Commons Attribution-Share Alike 3.0 lizentzia Creative Commons Attribution-NoDerivs 2.5 lizentzia Creative Commons Attribution-NonCommercial 2.5 lizentzia Creative Commons Attribution-NonCommercial-NoDerivs 2.5 lizentzia Creative Commons Attribution-NonCommercial-ShareAlike 2.5 lizentzia Creative Commons Attribution-ShareAlike 2.5 lizentzia Sortzailea: Kroaziera  Txekiera D Daniera Data: Jakin  behar duzu Jakin behar duzu iDevicearen bidez, ikaslearentzat derrigorrezkoak diren ezagutzak jorratzeko testuak sortu ditzakezu. 1. mailako nodoen izen lehenetsia 2. mailako nodoen izen lehenetsia 3. mailako nodoen izen lehenetsia Ezabatu Ezabatu artxiboa Ezabatu irudia Ezabatu aukera Ezabatu galdera Nodoa hierarkian jaitsi Descartes Deskribatu ikasleek nola ebaluatuko duten
ariketa egin duten modua. (Epigrafeak erabilgarriak dira 
hausnarketaren feedbacka emateko.) Emandako kasu istoriaren ataza nagusiak deskribatu. 
Galderak edota jarduera bat egiteko instrukzioak izan daitezke, 
ikasleak gatazka konpon dezan.  Ikasleek atazak behar bezala egiteko 
aurrez jakin beharko luketena deskribatu. Ikasleek irakurri beharko luketen testuarekin lotura duten atazak deskribatu. 
Lagungarriak izango dira garrantzia ikusarazteko. Deskribatu ikasleek bete beharko lituzketen atazak. Deskripzioa Deskripzioa: Nabarmendua iDevicearen bidez, ikaslearen arreta lortzeko testua nabarmendu dezakezu. Developing Nations 2.0 Barne esteka guztiak ezgaitu Ezgaitu Auto Top barne estekak Erakutsi honela: Ez kopiatu jatorrizko testua xedean Egina Dublin Core Dublín Core metadatuak Nederlandera Wikipediako artikulua nederlanderaz Dzongkha; Bhutanera ERROREA Element.process %s klasearekin deitu dute zuzenean ERROREA Element.renderEdit %s klasearekin deitu dute zuzenean ERROREA Element.renderView %s klasearekin deitu dute zuzenean ERROREA: Block.renderViewContent zuzenean deitu dute EZ DA ESPORTATU! EZ DA ESPORTATU!
%s EZ DA ESPORTATU!
%s da ondo esportatutako azken elementua. EZ DA ATERA!
%s Editatu Kargatzen ari zaren idevices/generic.data artxiboa edo paketea eXe-ren bertsio berriago batekin sortuta dago. Mesedez, eguneratu zure eXe-a eta berriz saiatu. Ebatzitako ariketa Enfasia Barne esteka guztiak gaitu Ingelesa Wikipediako artikulua ingelesez Idatzi 
artxiboarekin lotu nahi duzun testua. Idatzi arrasto bat. Ez baduzu
arrastorik eman nahi, hutsik utzi. Idatzi arrasto bat. Ez baduzu arrastorik eman nahi, hutsik utzi. Idatzi Wikipedian bilatu nahi duzun 
esaldia edo terminoa. Idatzi ikasleak hausnartzera bultzatzeko 
galderaren bat. Idatzi galeriaren izenburua Adierazi RSS URL bat zure edukira atxiki nahi 
duzun RSS feederako. Feedak ikono grafiko batekin identifikatzen dira 
maiz (horrelakoa <img src="/images/feed-icon.png" />), edo "RSS" testuarekin. Ikonoan 
edo testuan klikatzean, RSS feeda azalduko da nabigatzailean. 
RSSren URLa eremu horretan kopia eta pega dezakezu. Bestalde, eskuineko botoiaz 
estekan edo grafikoan klikatzean, menu bat irekiko da. Klikatu ESTEKAREN HELBIDEA KOPIATU 
edo lasterbidea kopiatu. Itzuli eXe-ra eta kopiatu esteka RSS lasterbidearen iDevice-a, eta itsatsi 
URLa RSS URL eremuan eta klikatu KARGATU botoia. Horrek zure feedetik izenburuak aterako ditu, 
eta edukian esteka gisa erakutsiko ditu. Hortik lastermarkak edita ditzakezu, eta argibideak edo 
informazio osagarria gehitu. Adierazi erantzun aukera bat. Eman 
hainbat distraitzaile (3 edo 4) eta erantzun zuzena. 
Klikatu "Gehitu beste aukera" beste erantzun bat gehitzeko. Adierazi ikasleei eman nahi diezun edozein 
feedback. Eremua hutsik utz dezakezu. Hori eginez gero, 
lehenetsitakoa emango da. Adierazi feedback eremuan ikasleei eman nahi diezun feedbacka. Eremua hutsik utz dezakezu. Adierazi hemen argibideak Adierazi erakutsi nahi duzun URLa
eta aukeratu erakutsi beharreko eremuaren tamaina. Adierazi hemen aukerak. 
Aukerak gehitzeko, klikatu "Gehitu beste aukera" botoia. 
Aukerak ezabatzeko, klikatu aukera bakoitzaren ondoan dagoen "X" botoi gorria. Adierazi hemen aukerak. 
Aukerak gehitzeko, klikatu "Gehitu beste aukera" botoia. 
Aukerak ezabatzeko, klikatu aukera bakoitzaren ondoan dagoen "X" botoi gorria. Adierazi irakurketaren zehaztasunak, erreferentziak barne. 
Erabilitako erreferentziaren estiloa zure fakultatearen edo sailaren 
lehentasunen araberakoa izango da. Adierazi flash artxiboaren neurria (pixeletan) 
eta lerrokatzea zehaztu. 
Luzera eta zabalera proportzionalki aldatuko dira. Adierazi hemen argibideak Sartu hemen etiketa Adierazi erro galdera. 
Argia eta zalantza gabekoa izan behar du. Saihestu premisa negatiboak 
anbiguoak izan ohi baitira. Adierazi erro galdera. 
Galdera argia eta zalantza gabekoa izan behar du. Saihestu premisa negatiboak 
ikasleak nahasten baitituzte. Adierazi irudiarekin lotu nahi duzun
                                     testua. Adierazi flash artxiboarekin lotu nahi duzun
                                     testua. Adierazi artxiboarekin lotu nahi duzun  
testua. Adierazi deskargatutako artxiboarekin lotu nahi duzun testua. Artxiboa deskargatu ondoren ikasleak zer egin behar duen edo materiala nola erabili behar duen azaldu dezake. Errorea XLIFF inportatzen: %s Esperantoa Estoniera  Wikipediako artikulua euskaraz Adibidea Adibidea Adibidea Esportatu <input_file> elp paketea esportatu hautazko <output_file>-ra hauetako formatu batean: xml, scorm, ims, website, webzip, singlepage edo xliff. Esportazio formatua ez dago inplementatua Esportatu iDevice-a %s-era esportatua Kanpoko webgunea Oso handia Atera paketea FPD - Aukera anitzetako jarduera FPD - Hutsuneak Betetzeko Jarduera FPD - Hutsuneak Betetzeko Jarduera (Eraldatua) FPD - Hautaketa Askotako Jarduera FPD - Egia/Gezurra Moduko Jarduera  FPD - Kasu Praktikoa FPD - Hausnarketarako Aipua FPD - Zuzenketa FPD - Jakin  Behar Duzu FPD - Nabarmendua FPD - Ebatzitako Ariketa FPD - Ikasle-Orientabideak FPD - Tutoretza-Orientabideak FPD - Gehiago Jakiteko FPD - Gomendioa FPD - Hausnartu (Feedbackarekin) FPD - Hausnartu (Feedbackik gabe) FPD - Testu Librea Gezurra. Faroera Feedbacka Eremu honetan daturik sartzen ez bada, 
ez da feedbackaren botoia agertuko. Feedbacka: Fijiera; Fiji %s artxiboa ez dago edo ezin da irakurri. '%s' fitxategia ondo inportatu da '%s'-ra. %s izeneko beste artxibo bat dago, eta ezin da ordeztu Artxiboaren izena: Finlandiera Flash bideoa Testudun flasha Dagoeneko badago %s izeneko karpeta bat. Mesedez, beste izen bat aukeratu edo dagoena ezabatu, eta berriz saiatu. Orri-oina: <output_file>-ren berridazketa behartu Formatua: Markoaren altuera: Testu librea Frantsesa Wikipediako artikulua frantsesez Frisiera GNUren dokumentazio librearen lizentzia Galiziera Geogebra Georgiera Alemana Wikipediako artikulua alemanez Lortu puntuak Atzera Aurrera Aurrera (ez dago eskuragarri) Greziera. Wikipediako artikulua grezieraz Guaraniera Gujaratera Hausa (?). Izenburuaren atzealdea: Hebreera (antzina iw).  Hereroa Ezkutatu Ezkutatu feedbacka Ezkutatu/Erakutsi hitza Hindia Arrastoa Hiri Motua Hasiera Hungariera IDevice ikonoa IDevice galderaren ikonoa IDevice kaltetua IEEE LOM IMS 1.1.3 edukia duen paketea Islandiera Ikonoak Identifikatzailea: Idoa Aukera hau klikatzen baduzu, inportazio prozesuak jatorrizko hizkuntzatik hartuko ditu testuak helburu hizkuntzatik hartu ordez. Aukera hau ez baduzu klikatzen, helburu testua hutsik sortuko da. Itzulpen tresna batzuk (adib.: OmegaT) xede hizkuntzako testuekin lan egiten dute soilik eta jatorrizko eremuaren testuarekin aurre-beteta egotea behar dute. Irudia Irudien bilduma Irudiak handitzekoa Testua duen irudia Inportazio formatua ez dago inplementatua Jatorrizko hizkuntzatik inportatu Inportatu iDevice-a <input_file>-etik <output_file> elp paketera inportatu hauetako formatu batean: xml edo xliff. Orri bakarreko esportazio fitxategia sartu Orri bakarreko esportazio fitxategia sartu? Webguneko esportazio fitxategiak sartu Webgune esportazio fitxategiak sartu? Aukera ez zuzena. Okerra. Indonesiera (antzina in) Info Informazioa Baliabidearen gaineko eskubideei buruzko informazioa. Hasierako zooma Sartu paketea Oraintxe zuzendu nahi? Argibideak Ikasleentzako argibideak Interlingua  Interlinguea Barne estekak (webguneak esportatzeko soilik) iDevice honetan agertuko den testua sartu Inuktituta Inupiaka Sarrera paketea ez da baliagarria Irteera paketea ez da baliagarria '%s' Irlandera Ondokoaren materiala erabiltzen du: Italiera Wikipediako artikulua italieraz JClic Japoniera Wikipediako artikulua japonieraz Java appleta  Javanera Jose Ramon Jimenez Reyes Kalaallisuta; Groenlandiera Kannada Kaxmirera Kazakhera Khmer; Kanbodiera Kikuyua Kinyaruanda Kirgizera Komoreera Koreera Kuanyama Kurduera Etiketa: Hizkuntza: Laosera Handia Latina Letoniera Ezkerra Luxenburgera 1. maila: 2. maila: 3. maila: Lizentzia: Ondoko lizentziak babestua: Lingala Lituaniera Kargatu Kargatu irudia %s artxibo lokala ez da aurkitu eta ezin da aurreikusi MP3 Mazedoniera Wikipediako artikulua hungarieraz Malaysiera Malabarera Malaialamera Maltera Manxera Maoriera Marathera Marshallera Matematika Ahalik eta gehien handitu Ertaina Metadatuak Ezin izan da MimeTeX bildu!
%s Moldaviera Mongoliera Puntu-kopurua erakutsi? Beherantz mugitu Irudia ezkerrerantz mugitu Irudia eskuinerantz mugitu Gorantz mugitu Nodoa beherantz mugitu Nodoa gorantz mugitu Aukera anitzak Hautaketa askotakoa N Izena Nauruera Navajoa Ndebelera, Iparra Ndebelera, Hegoa Ndongera Nepalera iDevice berria Hurrengoa Ez duzu adierazi --export edo --import aukera. Ez da irudirik kargatu Ez dago Thumbnail erabilgarririk. Ezin da jatorrizko irudia kargatu. Ez dago Thumbnail erabilgarririk. Ezin da irudia txikitu. Ez dago Thumbnail erabilgarririk. Ezin da jatorrizko irudia txikitu. Enfasirik ez Ez duzu adierazi sarrera fitxategia. Bat ere ez Sami Iparra Norvegiera Norvegiera, Bokmi Norvegiera, Nynorsk O Ados Helburuak Helburuek ikasketaren bidez lortu nahi diren xedeak deskribatzen dituzte, eta
ikasleek amaieran zertarako gai izan beharko luketen adierazi beharko lukete. Okzitanera; Proventzera Behin aukeratu duzula SCORM formatura esportatzea '-x scorm' edo '--export scorm' bitartez, ondoko esportazio aukerak erabili ditzakezu: Behin aukeratu duzula XLIFF formatura esportatzea '-x xliff' edo '--export xliff' bitartez, ondoko esportazio aukerak erabili ditzakezu: Behin aukeratu duzula XLIFF formatutik inportatzea '-i xliff' edo '--import xliff' bitartez, ondoko inportazio aukerak erabili ditzakezu: .flv (Flash Video Files) luzapena duten artxiboak bakarrik aukeratu 
iDevice honetarako. .swf (Flash Objects) luzapena duten artxiboak bakarrik aukeratu 
iDevice honetarako. Aukera Aukerak --export eta --import aukerak kontraesa sortzen dute. Ikasle-orientabideak idevicearen bidez irakasleak ezagutuko ditu ikaslearen ikasketa-helburuak. Ikasleentzako orientabideak Tutoretzarako orientabideak Ikasle-orientabideak iDevicearen bidez ikasleak ezagutu ditzake bere ikasketaren helburuak. Oriya Osetiera Beste bat Ingurua Nire gunea P Paketea Proiektuaren ezaugarriak Hurrengora ateratako paketea: %s Paketea zaharra da. Beste pakete batean sartzen saiatu aurretik, eguneratu mesedez (Artxiboa..Ireki...Honela gorde)! Hurrengoan gordetako paketea: %s Paliera Punjabera Gehiago jakiteko iDevicearen bidez, ikaslearentzat derrigorrezkoak ez diren ezagutzak jorratzeko testuak sortu ditzakezu. Gehiago jakiteko iDevicearen bidez, irakaslearentzat proposatu ditzake prestakuntzarako baliagarriak diren liburuak, pelikulak, ... Gehiago jakiteko Paxtuera Laguntza pedagogikoa Aholku pedagogikoa Persiera Mesedez, idatzi idevice izena. Mesedez, idatzi <br />idevice izena. Mesedez, aukeratu .flv artxibo bat. Mesedez, aukeratu .jpg artxibo bat. Mesedez, aukeratu galdera bakoitzaren erantzun zuzena. Mesedez, URL onargarri bat idatzi edo pegatu. Mesedez, kargatu .ggb artxibo bat. Mesedez igo jclic.zip fitxategia. Mesedez igo .sb edo .scratch fitxategia. Mesedez, eXe ixteko,
erabili 
Artxiboa...Irten. Mesedez, itxaron dena kargatu arte Honela bete: eszena zenbakia,eszena barneratzen duen URL-a (espaziorik gabe), adib.: 3,http://adibidea.com; gero Igo botoia sakatu. Poloniera Wikipediako artikulua polonieraz Wikipediako artikulua portugesez Portugesa Lehentasunak Aurreko ezagutzak Aurreko ezagutzak ikasleek ikasketak osatu ahal izateko aurrez jakin beharrekoak dira.
Horren adibideak dira: <ul>
<li>        Ikasleek ingeleseko 4. maila gaindituta izan behar dute </li>
<li>        Ikasleak oinarrizko tresnak muntatzeko gai izan behar du </li></ul>
 Aurrebista %s direktorioaren aurrebista artxibo bat da, eta ezin da ordeztu Aurrekoa Baliabidearen egile nagusia. Proiektuaren ezaugarriak Proiektuaren izenburua: Nodoa hierarkian igo  Ezaugarriak Eman izen bat MP3 artxiboari. 
Izen hori irakurgailuaren izenburuan ere agertuko da. Eman izen bat handitu beharreko 
irudiari. Eman izen bat gehitu berri duzun
flash filmari. Eman izen bat gehitu berri duzun
flash artxiboari. Eman izen bat gehitu berri duzun 
irudiari. Eman Egia/Gezurra galderak erantzuteko argibideak. Eman hutsuneak betetzeko jarduera egiteko argibideak. 
Eremuan aldaketarik egiten ez bada, lehenetsitako testua txertatuko da.
 Eman egoerari buruzko feedback 
nabarmena. Argitaratzailea: Asmoa Sartu hemen ikasleentzako argibideak Q Kitxua Galdera Galdera: Irten R RSS Irakurri beheko paragrafoa eta bete hutsuneak Irakurri beheko paragrafoa eta bete hutsuneak. Irakurtzeko jarduera Irakurtzeko 0.11 jarduera Gomendioa Hausnarketa Hausnarketa teoria eta praktika lotzeko maiz
erabiltzen den irakaskuntza metodoa da. Hausnartzeko atazek 
beren iritzia azaltzeko aukera ematen diete ikasleei, horiek lan batean jaso aurretik. 
Egunkariak, profilak eta karpetak tresna erabilgarriak dira
behaketa datuak biltzeko. Epigrafeak eta gidak feedback tresna 
eraginkorrak izan daitezke. Hausnartzeko galdera: Hausnartu Harremana: Berrizendatu Berriz hasi Feedbacka Erretorromaniera Eskuina Eskubideak: Errumaniera Kirundia Errusiera S EZ DA GORDE! EZ DA GORDE!
%s EZ DA GORDE!
%s da ondo gordetako azken elementua. SCORM 1.2 SCORM 1.2-ren aukerak SCORM galdera SCORM esportazioaren aukerak BIDALI ERANTZUNAK Samoera Sangoa Sanskritoa Sardiniera Gorde Eskoziera; Gaelikoa Scratch Atala Aukeratu flash objektua Aukeratu formatua Aukeratu hizkuntza Aukeratu lizentzia Aukeratu MP3 artxiboa Aukeratu formatua. Aukeratu artxibo bat Aukeratu flash bideo bat Aukeratu letra neurria:  Aukeratu hizkuntza. Aukeratu lizentzia. Aukeratu MP3 bat Aukeratu irudia Aukeratu irudia (JPG artxiboa) Aukeratu behar beste erantzun zuzen, 
aukera bakoitzaren ondoko koadroa markatuta. Aukeratu gainditze-koefizientea:  Jatorrizko hizkuntza aukeratu Xede hizkuntza aukeratu Aukeratu Wikipediako hizkuntza, 
eta sartu bilatu beharreko terminoa. Aukera zuzena hautatzeko, 
markatu botoia. Aukeratu luparen bidez handituko den neurria Auto-edukitako karpeta Serbiera Sothoera, Hegoa IDevice kargatzean, zoomaren hasierako maila jarri 
irudiaren jatorrizko neurriaren ehuneko gisa Zoomaren maila handiena 
jarri irudiaren jatorrizko neurriaren ehuneko gisa Ezarpenak gordeta Xoshera Erakutsi %s irudia Erakutsi erantzunak Erakutsi feedbacka Erakutsi irudia Erakutsi/Ezkutatu erantzunak Erakutsi/Ezkutatu feedbacka Sindhia Sinhala Gunea Luparen neurria:  Eslovakiera Esloveniera Wikipediako artikulua eslovenieraz Txikia Ebazpena Somaliera Enfasi pixka bat URL-a ez da irisgarria Artxiboaren formatua okerra da Artxiboaren formatua okerra da. Artxiboaren formatua okerra da:
%s Iturria: Espainiera Wikipediako artikulua espainieraz Istorioa: Zorroztasunez zuzendu? Gaia: Bidali Bidali erantzuna '%s'-ra ondo esportatu da '%s'-tik. Sundanera Swahilia Swaziera; Siswatiera Suediera Wikipediako artikulua suedieraz Tagaloa Tahitiera Tajikera Tamilera Tatarrera Taxonomia Irakasleek ohar hauek gogoan izan beharko lituzkete iDevice hau erabiltzean: <ol><li>Pentsatu zenbat ekintza mota erabili nahi dituzuen bisualki edukian. Ez erabili jarduera mota edo sailkapen gehiegi, ikasleentzat nahasgarria izan baitaiteke. Askotan, nahikoak dira hiruzpalau mota.</li><li>Diseinu bisualaren ikuspuntutik, saiatu bi iDevice elkarren ondoan ez jartzen, tartean testurik ez dutela. Beharrezkoa izanez gero, iDevice bakarrean sartu bi galdera edo ekintza.</li><li>Prestatzen dituzun jarduerei esker ikasleek lortutako onurak handiagoa izan behar du ekintza gauzatzeko emandako denborak eta ahaleginak baino. </li></ol> Telugua Testua Testuaren kaxa Testuaren lerroa Kasu praktikoaren testua: Hausnarketarako aipuaren testua: Zuzenkata testua: Jakin behar duzunaren testua: Nabarmendutako testua: Ikasle-Orientabidearen testua Ikasle-orientabideen testua: Tutoretza-Orientabideen testuak Tutoretza-orientabideen testua: Gehiago jakiteko testua Gehiago jakiteko testua Gomendioaren testua: Hausnarketarako aipuaren testua: Thailandiera MP3 iDevice-ak edukiari MP3 artxibo bat gehitzeko aukera ematen dizu, ikasgai garrantzitsuekin. iDevice RSS banako bati edukia
emateko erabiltzen da. iDevice honen bidez,
feed batetik estekak aukera ditzakezu, ikasleek ikus ditzaten. Atxikipenen iDevice hau zure .elp edukian aurretik dauden artxiboak atxikitzeko erabiltzen da. Adibidez, posible da ikasleei erakutsi nahi diezun PDF artxibo bat edo PPT aurkezpen bat izatea. Horiek atxiki egin daitezke, eta etiketak jar dakizkieke, zer diren eta zer tamaina duten adierazteko. Ikasleek atxikipenaren esteka klikatuz deskargatu ahal izango dute artxiboa. Baliabidearen edukiaren hedadura edo irekitasuna. Kanpoko webgunea ideviceak kanpoko web gune bat kargatzen du 
eXe-ren edukiaren markoaren barruan, beste leiho batean (popup) ireki beharrean. 
Beraz, ikasleek ez dituzte beste leiho batzuk erabili behar. 
iDevice hau erabili beharko duzu konektatutako ikasleek 
zure edukia ikustea nahi baduzu. Testudun flash iDevice-ak ahalbidetzen dizu flash artxibo bati 
informazio osagarria gehitzen- Erabilgarria izan daiteke 
ikasleei artxiboaren inguruko informazio osagarria emateko. Lupa irudiak handitzeko tresna da, ikasleek emandako irudiak
handi ditzaten. Lupa irudien gainean mugitzean, irudiak zehaztasunez azter daitezke. Ikasteko baliabide baten egiteko nagusiak testuingurua zehaztea,
instrukzioak ezartzea eta informazio orokorra ematea da.
Hala, ikasketa jarduerak sortzeko eta emateko markoa sortzen da. LATEX lengoaia matematikoa edukian formula 
                      matematikoak sartzeko aukera izan dezazun erabili da.
                      Horretarako, LATEXa irudi bilakatzen da, eXe edukian erakusten dena.
                      Testu libreko iDevice-a erabiltzea gomendatzen dizugu oharrak eta
                      grafikoei buruzko azalpenak emateko. Baliabideari emandako izena. Baliabidearen edukiaren izaera edo generoa. Proiektuaren titulua. Dialogoak balizko erabiltzaileei iDevice-ren inguruan duzun asmoa azaltzeko aukera ematen dizu. Baliabidearen edukiaren gaia. Artikulu honek ondoko lizentzia du: Honekin, luparen hasierako 
neurria aukeratzen da iDevice honek Flash Video File (.FLV) formatua onartzen du, eta
ez besterik. Dena den, beste bideo formatu batzuk
(adib., mov, wmf) .FLV formatu bihur ditzakezu, hirugarrenen
kodifikatzaileen bidez. Horiek ez datoz eXe-rekin batera. Bestalde,
bideoak ikusteko 8 player deskargatu beharko dute erabiltzaileek,
hemen: http://www.macromedia.com/. Testu eremu librea da hau. Ikasgai orokorra idatz daiteke. Honako hau testu eremu librea da. Honako hau erabiltzaile batek sortutako
iDevice plugin baten adibidea da. Eremu hau aukerazkoa da. Aukera honek, esportatutako eremuak bilduko ditu CDATA ataletan. XLIFF estandarrak ez du gomendatzen atal hauen erabilera baina aukera interesgarri bat izan daiteke aurre-prozesaketa tresna bat erabili nahi baduzu (adib. Rainbow) itzulpen tresna erabili aurretik. Honek IDevice-a ezabatuko du.
Hori egin nahi al duzu? Tibetera Tigriniera Atzekaldeko irudia zehaztu? Aholkua: Izenburua Izenburua: Erantzun zuzena markatzeko, 
klikatu aukera zuzenaren ondoko botoia. Tongera Gaia Egia. Egia/gezurra galderak  Egia/gezurra galderetan, adierazpen bat azaltzen da, 
eta ikasleak egia edo gezurra den erabaki beharko du. Galdera mota hori 
egokia da gertaeren informazioa emateko eta biak/edo motako erantzunetarako. Tsongera Tswanera; Setswanera Turkiera Turkmenera Tuiera Idatzi hemen eztabaidarako gaia. Idatzi ikasleek aukera bat hautatzean ikusiko duten 
feedbacka. Ez baduzu kutxa bete nahi, eXe-k lehenetsitako 
ondoko feedbacka emango du: "Erantzun zuzena" 
,aukeratutakoa ondo badago, edo "Erantzun okerra" 
,beste erantzunetan. Ikasleak galdera zehatz bat markatzen duenean,
hark ikusi beharreko informazioa idatzi. Ez baduzu kutxa betetzen,
 eXe-k automatikoki feedback hau jarriko du: "Erantzun zuzena"
erantzuna zuzena bada; edo "Erantzun okerra" beste aukeretan. Idatzi ikasleari eman nahi diozun 
feedbacka. Idatzi baliabide honen helburu pedagokikoak. Idatzi galdera. Galderak argia eta zalantzarik gabekoa
izan beharko luke. Saiatu ezezko premisak ez erabiltzen,
anbiguoak izaten baitira. Mota: Eskubideetako elementu batek, normalean, baliabidearen eskubideen inguruko adierazpena izango du, edota informazio hori emango duen zerbitzuari egingo dio erreferentzia. Eskubideen informazioa, askotan, jabetza intelektualaren eskubideak, copyright-ak eta bestelako jabetza eskubideek osatzen dute. Eskubideen elementurik ez badago, eskubide guztiak erreserbatuta daudela ulertuko da. U URLa: Uigurra Ukraniera Ezin izan da %s-etik deskargatu <br/>Mesedez, egiaztatu ortografia eta konexioa, eta berriz saiatu. Ezin da RSS feed-a %s-tik kargatu <br/> Mesedez, egiaztatu ortografia eta konexioa, eta berriz saiatu. Ezin da konbinatu: Java Applet baliabide izena bikoiztua dago (barnean " Edizioak desegin Unitatea Aucklandeko Unibertsitatea MCQa ez bezala, SCORM ariketa ikasleei 
informaziorik eman gabe haien jakintza egiaztatzeko erabiltzen da.
Ariketa hau ikasleek informazioa ikasteko eta
erabiltzeko denbora izan ondoren egiten da askotan. Eguneratu zuhaitza Kargatu Urdua Erabilera: %prog [options] input_file [output_file]

Laguntza erakusteko:
%prog -h Feedbacka erabili irakurgaian jorratutako puntuen laburpena egiteko 
edo irakurgaiaren azterketa sakonagoa egiteko Erakusteko irudi asko badituzu erabili iDevice hau. Zure asmoak eta baliabidearen helburu pedagogikoak azaltzeko erabili eremu hau. Erabilgarria izan daiteke zure iDevice-a beste batzuei esportatu behar izanez gero. Testua idazteko erabili eremu hau. iDevice honek 
ez du enfasirik, baina testuari formatu mugatua aplika dakioke,
eremuari lotutako edizio botoiekin. iDevice-ak formatua izan behar duen edo ez aukeratzeko erabili zerrenda hau; adibidez, ertza edo ikonoa. Uzbekera VK_EZABATU VK_TXERTATU VK_EZKERRA VK_ESKUINA Vietnamera Volapuka Walloiera Webgunea Web orrien oina. Galesa Zer irakurri MCQak sortzean ondokoak kontuan hartu:<ul>
<li> lkasleek ezagutzen dituzten eta ikasketetan aurkitu 
dituzten esaldiak erabili </li>
<li> Erantzun laburrak eman </li>
<li> Galderaren eta erantzunen artean nolabaiteko lotura egon behar du </li>
<li> Nahikoa aukera eman, ikasleek erantzuna pentsa dezaten
</li>
<li>Erantzun zuzenak distraitzaileak baino zehatzagoak ez izatea</li>
<li>Distraitzaileak sinesgarriak izan behar dira</li>
</ul>
 Wiki artikulua Wikiburuetako artikulua Wikihezitzaileko edukia Wikibertsitatea Wikiztegia Wolofera Eremuak CDATA sailetan bildu Okerra. XHTML XLIFF esportazioaren aukerak XLIFF inportazioaren aukerak Xhosera Yiddishera (antzina ji). Jorubera Testu barra erabili edota latexa eskuz sar dezakezu testu koadroan. Zure iDevice berriak izenburu hori izango du iDevice panelean. Eremu hau derrigor bete beharrekoa da. IDevice-a ezer jarri gabe bidaltzen saiatzen bazara, etiketa bat idazteko eskatuko zaizu. Zuangera Artxibo konprimitua Zuluera artikulua hutsik jatorrizko neurrirako debugInfo eXe eXe: XHTML editorea urruneko hezkuntzarako eXe proiektua eXeex -eri buruz exe_do: errorea: Ezin izan da esportatu '%s'-tik.
Errorea izan da: exe_do: errorea: Ezin izan da inportatu '%s'.
Errorea izan da: exelearning %s artxiboak ez du ahaide nodorik http://eu.wikipedia.org/ %s iDevice-k ez du paketerik iDevice editorea iDevice ikonoa IDevice-aren beira IDevice-ak label="eXe-ri buruz" accesskey="a" label="Common Cartridge" accesskey="c" label="Esportatu" accesskey="e" label="Atera paketea" accesskey="E" label="Artxiboa" accesskey="f" label="HTML ikastaroa" accesskey="h" label="Laguntza" accesskey="h" label="IMS edukia duen paketea" accesskey="i" label="Inportatu" accesskey="i" label="Txertatu paketea" accesskey="i" label="Konbinatu" ccesskey="m" label="Berria" accesskey="n" label="Ireki" accesskey="o" label="Inprimatu" accesskey="p" label="Irten" accesskey="q" label="Azken proiektuak..." accesskey="r" label="Eguneratu bista" accesskey="r" label="Bertsioaren oharrak" accesskey="n" label="Argitaratu gai bat" accesskey="r" label="SCORM 1.2" accesskey="s" label="Gorde honela..." accesskey="a" label="Gorde" accesskey="s" label="Orri bakarra" accesskey="p" label="Estiloak" accesskey="s" label="Testu artxiboa" accesskey="t" label="Tresnak" accesskey="t" label="XLIFF fitxategia batu" accesskey="x" label="XLIFF" accesskey="x" label="eXe-ren txata" accesskey="c" label="eXe-ren gidaliburua" accesskey="m" label="eXe-ren tutoriala" accesskey="u" label="eXe-ren webgunea" accesskey="w" label="iPod oharrak" accesskey="n" lerro berria Inguruko beira Applet Idevice baten baliabide baten izena aldatu da eta ez luke honeraino iritxi behar! erakutsi laguntza mezu hau eta atera testua 