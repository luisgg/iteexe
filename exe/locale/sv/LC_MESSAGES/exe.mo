��    �     �  K  <      h$     i$     w$  Q  �$  �  �%    |'     �(  f  �(  
   �+     ,     ,  	   ,  	   ,     ),     8,  "   A,  i   d,  X   �,  '   '-  2   O-      �-     �-     �-     �-     �-     �-  	   .  
   .     .  
   #.  	   ..     8.  Y   ?.  1   �.     �.  �   �.     �/     �/  	   �/     �/  	   �/  
   �/     �/      0  	   0     0     0     #0     00     80  
   A0     L0     U0     d0     k0     �0  
   �0     �0     �0     �0     �0     �0     �0     �0     
1     1  Z   )1  
   �1  
   �1  ^   �1     �1     �1     2  
   2  	   2     )2     12     @2  	   I2  	   S2     ]2  	   f2     p2     r2     y2     �2     �2     �2  �   �2  _   83  0   �3     �3     �3     �3     �3     �3  3   4  6   ?4  6   v4  .   �4     �4     �4     �4  4   5  P   95  P   �5  /   �5  �   6  l   �6  &   7  �   57  �   �7  �   s8  *   9     69  e   K9  V   �9  4   :  	   =:     G:     N:     ]:     l:     ~:     �:     �:     �:     �:     �:     �:  	   �:  (   �:  	   ;     ;     ;     .;  	   6;     @;     Y;  	   b;     l;     t;  	   �;     �;     �;  	   �;     �;     �;     �;     �;     �;     �;  
   �;      <     <     #<     2<     L<     R<     ^<     d<     r<     �<     �<  	   �<  
   �<     �<     �<     �<     �<     �<     �<     
=     =      =     :=     U=     [=     c=  	   j=     t=     �=     �=     �=     �=     �=     �=     �=     �=     �=  
   �=  -   �=     >     >     '>     0>     7>     @>  
   F>  
   Q>  	   \>     f>     v>     �>     �>     �>     �>     �>     �>     �>     �>     �>     �>  6   �>  /    ?  8   P?     �?     �?  
   �?     �?     �?     �?     �?  
   �?  �   �?     y@     �@     �@     �@     �@     �@     �@     �@     �@     �@      A     	A  "   'A     JA     eA  1   �A     �A     �A     �A     �A  0   B     1C  1   9C     kC     tC     �C  
   �C  1   �C  >   �C  Z   D  8   mD     �D  "   �D     �D  	   �D     �D     �D  6   �D  7   "E     ZE     kE  
   �E  }  �E     
G  
   G  	   *G     4G     ;G     AG  	   IG     SG     \G  	   ^G     hG  
   zG     �G     �G  
   �G     �G     �G     �G     �G     �G     �G     �G     H     H     $H     9H     NH     aH     sH     �H     �H  _   �H     I     I     (I     /I     =I     JI  
   XI     cI     vI     �I  
   �I     �I     �I  
   �I     �I     �I     �I     �I     �I     �I     J     (J     EJ     MJ     gJ     nJ     wJ     ~J  
   �J     �J     �J  	   �J     �J     �J     �J     �J     �J     �J  	   �J     �J  h   K  �   iL     BM     bM  j   wM  B   �M     %N     @N  =   [N     �N     �N     �N     �N  S   �N     O     O     O     O  �   0O     P     P     P     P  *  ;P  1  fQ  /   �R     �R     �R  
   �R     �R     �R     �R  �   �R  5   �S  �   �S  	   �T  	   �T     �T     �T     �T     �T     �T     �T     �T     U  �   U     �U     �U  	   �U     �U     �U     V     *V     CV     ]V     lV     yV     �V     �V  &   �V     �V  %   �V     W  !   3W     UW  )   pW     �W  $   �W     �W     �W     X     /X     KX  (   fX  %   �X  #   �X  %   �X     �X      Y     @Y  !   [Y     }Y     �Y     �Y      �Y     �Y  #   Z      7Z  "   XZ  "   {Z      �Z     �Z     �Z     �Z  �  �Z     |\     �\  "  �\  %  �]  �   �^     �_  ,  �_     �`  
   �`  	   �`     �`     a     a     .a  /   >a  b   na  U   �a  /   'b  /   Wb  /   �b     �b      �b     �b     c      5c     Vc     hc     zc     �c     �c     �c  J   �c  J   �c     1d  �   9d     e     e     $e     ,e     4e     =e  !   De     fe  	   re     |e     �e     �e     �e     �e     �e     �e     �e     �e     �e     �e  
   �e     �e     �e  
   f     f     f     "f  
   4f  
   ?f  
   Jf  I   Uf     �f     �f  J   �f     g     	g     g     !g     (g     /g     5g     ;g     Bg  
   Jg     Ug     ]g     eg     gg     ng  
   ug     �g     �g  �   �g  ,   ,h  +   Yh     �h     �h  	   �h     �h     �h  )   �h  ,   �h  ,   i  -   Ci     qi  
   zi     �i  J   �i  Q   �i  Q   4j  I   �j  Z   �j  Z   +k     �k  �   �k  �   �l  �   �m     $n     =n  J   Qn  J   �n  J   �n     2o     :o     Mo     Yo     lo  )   }o  )   �o     �o  	   �o     �o     �o     �o     �o  ,   p     8p     Ap     Gp     Vp     ]p     ep     wp     p     �p     �p     �p     �p     �p     �p     �p     �p     �p     �p     q     q     q     q     *q     9q  )   Hq     rq     yq     �q     �q     �q     �q     �q     �q     �q  	   �q     �q  )   �q     r     r     /r     7r     ?r     Gr     Yr     kr     sr     {r     �r     �r     �r     �r     �r     �r     �r     �r     �r     �r     �r     �r  ,   �r     &s     .s     @s     Gs     Os     Ws     _s     gs  
   os     zs     �s  
   �s  
   �s  
   �s     �s     �s     �s     �s  
   �s     �s     �s  B   t  B   Xt  B   �t     �t     �t     �t     �t     u     u     u     u  �   u     �u     �u  	   �u  )   �u     �u     �u     v     *v     2v     Dv     Uv     ]v     vv     �v     �v  (   �v     �v     �v     �v     w    w     x  ,   0x     ]x     nx     �x  
   �x  J   �x  J   �x  J   7y  J   �y     �y     �y     �y     �y     �y     z  6   	z  6   @z     wz     �z  
   �z  �  �z     !|  
   2|  
   =|     H|     Q|     X|     _|     g|     o|     q|     �|     �|     �|     �|     �|     �|     �|     �|  
   �|      }     }  	   }  
   $}  	   /}     9}  	   H}     R}     _}  	   l}  
   v}  
   �}  L   �}     �}     �}     �}      ~     ~     ~     5~     C~     R~     j~     s~     z~     �~     �~     �~     �~     �~     �~     �~     �~     �~     �~     	          "     )     0     <     H     O     W     i     q     y     �     �     �     �  	   �     �  Q  �  �   �  !   ف     ��  h   �  K   v�          ߂  D   ��     A�     I�     O�     U�  L   [�     ��     ��     ��     ��  �   у     ��     ��     ��     ��  �   ��  �   ��  !   ��     Ȇ     Ά     І     ؆     ކ     ��    ��  6   �  t   ;�  	   ��  	   ��     Ĉ     ̈     Ո     ވ     �     ��     �     �  D   �     _�  !   g�  	   ��     ��  "   ��  !   ��     ܉     ��     �     �     .�     :�     F�  "   _�     ��  )   ��     ̊     �     �  )    �     J�  )   j�     ��     ��     ʋ     �      �     �     9�     Y�     r�     ��  "   ��     Ռ  "   �     �     1�     K�     i�     ��     ��     ��     ڍ     ��     �     0�     8�     D�     u   �  �  �   �   �   ~       +   �       1   �   x  4          /   �           �   �   9   �           V       �   m  ,   �   N   �      �              �   
   _           t          �   �  �       h    �  �   K  �   j  7   D   %             y  �   S   L  �       �   �           �  I   �   �   �  �      e   p      [     4   �   �   �  Q       n   -           �   �  @     S  g   �   �           �   �       F  !               %  �      C   �  �   �   C  �                �    D  0           O   �   ]        l  s          W  �       Z       r           ?   �   �        |  �   2   p   �   Y  �   �          �   8  3     �   �     I  :  �       �   �       l   �        �      ;        ^                    �      _  9  �   �   z   �       �      1     �       ^  �  |   
  �   �              �   i  ]   B   c      o  5   =   '   q   E   a   `          Q  U   k   �   �  �      �       �             �   n  e      -             �       P  �   �      �   �   �   �   �   g      Z    ~              {        �   �   �      ?  c                 �  U      }      r      �   +          �  @   x   O      �     <       #      �   �          "  �      �   �      �  �  b       $   "       7      �       �   �   �             �  J     G       �  {   v   3  �                    k  �     	  �     �   `      .     �   �   M       &  #               �           R  �   R   �     d      Y   A  \          *  �          �  �   T   �  h       L     6  >   b      A   �  <  T  �  *                      �   �   �  )  q  :       �       J      �   y       �   �   =      �   �       [   �  �   X   ,  �   �   B  \  �   �  �   N          o   H   !  �     �  �  �   )       �   �                m   �      �   M  �   �   .      �         V  j   �   �  8   �          �  �  �  �  a      �       H  $  �   �         G  &   �   �   �          s   >  �         f  6           �           5  �       �      '  �   i          �                 �   0      ;   }   f               t       �   (   �   K              �   W     2      X  �   d   �  E  �  z      �       P       �          	   w  (  �  �   u  v  F   /  w       �   ---Move To--- . Please use ASCII names. <p>Assign a label for the attachment. It is useful to include the type of file. Eg. pdf, ppt, etc.</p><p>Including the size is also recommended so that after your package is exported to a web site, people will have an idea how long it would take to download this attachment.</p><p>For example: <code>Sales Forecast.doc (500kb)</code></p> <p>Cloze exercises are texts or sentences where students must fill in missing words. They are often used for the following purposes:</p><ol><li>To check knowledge of core course concepts (this could be a pre-check, formative exercise, or summative check).</li><li>To check reading comprehension.</li><li>To check vocabulary knowledge.</li><li>To check word formation and/or grammatical competence. </li></ol> <p>Enter the text for the cloze activity in to the cloze field 
by either pasting text from another source or by typing text directly into the 
field.</p><p> To select words to hide, double click on the word to select it and 
click on the Hide/Show Word button below.</p> ????? A case study is a device that provides learners 
with a simulation that has an educational basis. It takes a situation, generally 
based in reality, and asks learners to demonstrate or describe what action they 
would take to complete a task or resolve a situation. The case study allows 
learners apply their own knowledge and experience to completing the tasks 
assigned. when designing a case study consider the following:<ul> 
<li>	What educational points are conveyed in the story</li>
<li>	What preparation will the learners need to do prior to working on the 
case study</li>
<li>	Where the case study fits into the rest of the course</li>
<li>	How the learners will interact with the materials and each other e.g.
if run in a classroom situation can teams be setup to work on different aspects
of the case and if so how are ideas feed back to the class</li></ul> Abkhazian  Actions Activity Add Field Add Image Add JPEG Image Add Page Add a flash video to your iDevice. Add a single text line to an iDevice. Useful if you want the ability to place a label within the iDevice. Add a text entry box to an iDevice. Used for entering larger amounts of textual content. Add an attachment file to your iDevice. Add an interactive feedback field to your iDevice. Add an mp3 file to your iDevice. Add another Option Add another Question Add another activity Add another option Add another question Add files Add images Afar  Afrikaans  Albanian  Align: Allow auto completion when 
                                       user filling the gaps. Allow auto completion when user filling the gaps. Amharic  An activity can be defined as a task or set of tasks a learner must
complete. Provide a clear statement of the task and consider any conditions
that may help or hinder the learner in the performance of the task. Answsers Arabic  Armenian  Article Assamese  Attachment Attachment %s has no parentNode Author: Authoring Avestan  Aymara  Azerbaijani  Bihari  Bosnian  Bulgarian  Burmese  Button Caption Cancel Cannot access directory named  Caption: Case Study Catalan  Catalan Wikipedia Article Change Image Chechen  Chinese  Chinese Wikipedia Article Choose an Image Choose an MP3 file Clear Image Click <strong>Select a file</strong>, browse to the file you want to attach and select it. Click Here Click here Click on Preview button to convert 
                                  the latex into an image. Close Cloze Cloze Activity Cloze Text Clozelang Correct Correct Option Correct! Corsican  Coverage: Creator: Croatian  D Delete Delete File Delete Image Delete option Delete question Describe how learners will assess how 
they have done in the exercise. (Rubrics are useful devices for providing 
reflective feedback.) Describe the prerequisite knowledge learners should have to effectively
complete this learning. Describe the tasks the learners should complete. Description Description: Display as: Done Dutch Wikipedia Article ERROR Element.process called directly with %s class ERROR Element.renderEdit called directly with %s class ERROR Element.renderView called directly with %s class ERROR: Block.renderViewContent called directly Edit Emphasis English Wikipedia Article Enter 
the text you wish to associate with the file. Enter a hint here. If you
do not want to provide a hint, leave this field blank. Enter a hint here. If you do not want to provide a hint, leave this field blank. Enter a question for learners 
to reflect upon. Enter any feedback you wish to provide 
to the learner. This field may be left blank. if this field is left blank 
default feedback will be provided. Enter any feedback you wish to provide the learner with-in the feedback field. This field can be left blank. Enter instructions for completion here Enter the available choices here. 
You can add options by clicking the "Add Another Option" button. Delete options 
by clicking the red "X" next to the Option. Enter the available choices here. 
You can add options by clicking the "Add another option" button. Delete options by 
clicking the red X next to the option. Enter the details of the reading including reference details. The 
referencing style used will depend on the preference of your faculty or 
department. Enter the instructions for completion here Enter the label here Enter the text you wish to 
                                                associate with the image. Enter the text you wish to 
                                 associate with the image. Enter the text you wish to 
associate with the file. Estonian  Export Export iDevice Exported to %s External Web Site Extra large Extract Package FPD - Correccion FPD - Texto Libre False Faroese  Feedback Feedback: Filename %s is a file, cannot replace it Filename: Flash Movie Flash with Text Format: Free Text French Wikipedia Article Frisian  Georgian  German  German Wikipedia Article Get score Greek Wikipedia Article Guarani  Gujarati  Hide Hide Feedback Hide/Show Word Hindi  Hint Home Hungarian  IDevice Icon IDevice Question Icon IDevice broken IMS Content Package 1.1.3 Icons Identifier: Image Image Gallery Image Magnifier Image with Text Import iDevice Incorrect Incorrect! Info Information Insert Package Instructions Instructions For Learners Interlingua  Interlingue  Italian  Italian Wikipedia Article Japanese Wikipedia Article Komi  Korean  Label: Language: Lao; Laotian  Large Latin  Left Level 1: Level 2: Level 3: License: Lithuanian  Load Load Image Local file %s is not found, cannot preview it Macedonian  Magyar Wikipedia Article Maltese  Maori  Marathi  Maths Moldavian  Mongolian  Move Down Move Image Left Move Image Right Move Up Move node down Move node up Multi-choice N Name Ndonga  New iDevice Next No Images Loaded No Thumbnail Available. Could not load original image. No Thumbnail Available. Could not shrink image. No Thumbnail Available. Could not shrink original image. No emphasis None Norwegian  Norwegian Bokmål  Norwegian Nynorsk  O OK Objectives Objectives describe the expected outcomes of the learning and should
define what the learners will be able to do when they have completed the
learning tasks. Option Options Outline Package Package Properties Package extracted to: %s Package saved to: %s Pali  Pedagogical Help Pedagogical Tip Persian  Please enter an idevice name. Please enter<br />an idevice name. Please select a .flv file. Please select a .jpg file. Please select a correct answer for each question. Polish Wikipedia Article Portugese Wikipedia Article Preferences Preknowledge Prerequisite knowledge refers to the knowledge learners should already
have in order to be able to effectively complete the learning. Examples of
pre-knowledge can be: <ul>
<li>        Learners must have level 4 English </li>
<li>        Learners must be able to assemble standard power tools </li></ul>
 Preview Preview directory %s is a file, cannot replace it Previous Project Properties Project Title: Properties Provide a caption for the 
image to be magnified. Provide a caption for the flash movie 
you have just inserted. Provide a caption for the flash you 
                                  have just inserted. Provide a caption for the image 
you have just inserted. Purpose Put instructions for learners here Question Question: Quit R Read the paragraph below and fill in the missing words Read the paragraph below and fill in the missing words. Reading Activity Reading Activity 0.11 Reflection Reflection is a teaching method often used to 
connect theory to practice. Reflection tasks often provide learners with an 
opportunity to observe and reflect on their observations before presenting 
these as a piece of academic work. Journals, diaries, profiles and portfolios 
are useful tools for collecting observation data. Rubrics and guides can be 
effective feedback tools. Reflective question: Reflexiona Relation: Rename Right Rights: Romanian  Russian  S SCORM 1.2 SCORM 1.2 Options SCORM Quiz SUBMIT ANSWERS Samoan  Sardinian  Save Section Select Flash Object Select Format Select Language Select License Select MP3 file Select a Format. Select a file Select a flash video Select a font size:  Select a language. Select a license. Select an MP3 Select an image Select an image (JPG file) Select as many correct answer 
options as required by clicking the check box beside the option. Select pass rate:  Serbian  Shona  Show %s Image Show Answers Show Feedback Show Image Show/Clear Answers Show/Hide Feedback Sindhi  Sinhalese  Site Slovak  Slovenian  Slovenian Wikipedia Article Small Solucion Somali  Some emphasis Sorry, wrong file format Sorry, wrong file format. Sorry, wrong file format:
%s Source: Spanish Wikipedia Article Story: Subject: Submit Submit Answer Sundanese  Swahili  Swedish Wikipedia Article Tahitian  Tajik  Tamil  Tatar  Taxonomy Text Text Box Text Line Thai  The attachment iDevice is used to attach existing files to your .elp content. For example, you might have a PDF file or a PPT presentation file that you wish the learners to have access to, these can be attached and labeled to indicate what the attachment is and how large the file is. Learners can click on the attachment link and can download the attachment. The majority of a learning resource will be 
establishing context, delivering instructions and providing general information.
This provides the framework within which the learning activities are built and 
delivered. The name given to the resource. The project's title. The purpose dialogue allows you to describe your intended purpose of the iDevice to other potential users. This is a free text field general learning content can be entered. This is a free text field. This is an optional field. This will delete this iDevice.
Do you really want to do this? Tibetan  Tip: Title Title: To indicate the correct answer, 
click the radio button next to the correct option. Tongan   Topic True True-False Question True/false questions present a statement where 
the learner must decide if the statement is true. This type of question works 
well for factual information and information that lends itself to either/or 
responses. Tsonga  Turkmen  Twi  Type a discussion topic here. Type in the feedback that you want the 
student to see when selecting the particular option. If you don't complete this 
box, eXe will automatically provide default feedback as follows: "Correct 
answer" as indicated by the selection for the correct answer; or "Wrong answer"
for the other options. Type in the feedback that you want the 
student to see when selecting the particular question. If you don't complete
this box, eXe will automatically provide default feedback as follows: 
"Correct answer" as indicated by the selection for the correct answer; or 
"Wrong answer" for the other alternatives. Type the learning objectives for this resource. Type: U Ukrainian  Unit University of Auckland Upload Use feedback to provide a summary of the points covered in the reading, 
or as a starting point for further analysis of the reading by posing a question 
or providing a statement to begin a debate. Use this Idevice if you have a lot of images to show. Use this field to describe your intended use and the pedagogy behind the device's development. This can be useful if your iDevice is to be exported for others to use. VK_DELETE VK_INSERT VK_LEFT VK_RIGHT Vietnamese  Web Site What to read Wiki Article Wikibooks Article Xhosa  Your new iDevice will appear in the iDevice pane with this title. This is a compulsory field and you will be prompted to enter a label if you try to submit your iDevice without one. article  blank for original size debugInfo eXe eXe : elearning XHTML editor file %s has no parentNode http://en.wikipedia.org/ iDevice %s has no package iDevice Editor iDevice icon iDevicePane iDevices label="About eXe" accesskey="a" label="Common Cartridge" accesskey="c" label="Export" accesskey="e" label="Extract Package" accesskey="E" label="File" accesskey="f" label="HTML Course" accesskey="h" label="Help" accesskey="h" label="IMS Content Package" accesskey="i" label="Import" accesskey="i" label="Insert Package" accesskey="i" label="Merging" accesskey="m" label="New" accesskey="n" label="Open" accesskey="o" label="Print" accesskey="p" label="Quit" accesskey="q" label="Recent Projects..." accesskey="r" label="Refresh Display" accesskey="r" label="Release Notes" accesskey="n" label="Report an Issue" accesskey="r" label="SCORM 1.2" accesskey="s" label="Save As..." accesskey="a" label="Save" accesskey="s" label="Single Page" accesskey="p" label="Styles" accesskey="s" label="Text File" accesskey="t" label="Tools" accesskey="t" label="XLIFF file" accesskey="x" label="XLIFF" accesskey="x" label="eXe Live Chat" accesskey="c" label="eXe Manual" accesskey="m" label="eXe Tutorial" accesskey="u" label="eXe Web Site" accesskey="w" label="iPod Notes" accesskey="n" newline outlinePane text Project-Id-Version: eXe trunk (pre-0.11)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-07-26 10:49+0200
PO-Revision-Date: 2006-07-28 15:37+1200
Last-Translator: Jenny <jennylin_nz@yahoo.com>
Language-Team: Fredrik Paulsson <frepa@frepa.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-EXE-Language: Svenska
X-Poedit-Language: Swedish
X-Poedit-Country: Sweden
 ---Flytta till--- . Använd ASCII namn. <p>Ge den bifogade filen en etikett. Det är bra att även ange filtyp. T.ex. pdf, ppt, etc.</p><p> Det är också bra att ange filens storlek för att ge användare en uppfattning om hur lång tid nerladdningen kommer att ta.</P> Exempelvis: <conde>Att göra en IUP.odt (500 kb)</code></p> <p> Avslutningstest är tester eller övningar där ord eller fraser har uteslutits. Eleven skall avsluta texten genom att fylla i det som saknas. Dessa tester kan exempelvis användas för att:</p><ol><li>Diagnostiska tester</li><li>Läsförståelse</li><li>Testa ordkunskap och gramatik</li> Fyll i texten till slutövningen genom att klistra in eller skriva i fältet. Markera ord som du vill dölja genom att dubbelklicka på ordet och klicka på <i>Visa/dölj ord</i> -knappen. ????? En case-studie är en berättelse som förmedlar ett 
pedagogisk budskap. En case-studie kan användas för att beskriva en realistisk situation 
som ger eleven möjlighet att tillämpa sin egen kunskap och erfarenhet. När 
du designar en case-studie kan det vara lämpligt att beakta följande:<ul> Rubrik: Åtgärder Aktivitet Lägg till bilder Lägg till bilder Lägg till sida Lägg till sida Lägg till interaktiv feedback till din iDevice Lägg till en textrad till en iDevice. Användbart om du vill lägga till en etikett i en iDevice. Lägg till ett textfält till en iDevice. Används för att fylla i beskrivande text. Lägg till interaktiv feedback till din iDevice Lägg till interaktiv feedback till din iDevice Lägg till interaktiv feedback till din iDevice Lägg till ytterligare ett val Lägg till ytterligare en fråga Lägg till ytterligare ett val Lägg till ytterligare ett val Lägg till ytterligare en fråga Lägg till bilder Lägg till bilder Artikel Rubrik: Rubrik: Justera: Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Artikel En aktivitet kan definieras som en uppgift eller som en serie av uppgifter som eleven måste
 göra. Beskriv uppgiften tydligt och överväg alla omständigheter
 som kan utgöra potentiella problem för eleven. Visa/göm svar Artikel Rubrik: Artikel Byt namn Bilaga Bilaga %s har ingen föräldranod Författare Författa Rubrik: Artikel Rubrik: Artikel Rubrik: Rubrik: Falskt Rubrik Avbryt Har ej tillträde till Rubrik: Fallstudie Rubrik: Wikipedia-artikel Välj bild Byt namn Byt namn Wikipedia-artikel Välj bild Välj bild Välj bild Klicka på <strong>Välj fil</strong>, navigera till filen och välj den. Klicka här Klicka här Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Stäng Stäng Stäng aktivitet Stäng Stäng Rätt Rätt Rätt! Rubrik: Välj bild Rubrik: Rubrik: D Radera Radera Välj bild Ta bort alternativ Ta bort fråga Beskriv hur eleverna skall få tillgång till
 sina resultat för en övning. (Rubriker är användbara för att ge
 reflektiv feedback.) Beskriv de förkunskaper som eleven bör ha. Beskriv den uppgift som eleven skall göra. Beskrivning Beskrivning: Visa som: Klar Wikipedia-artikel ERROR Element.process anropad direkt (%s) ERROR Element.renderEdit anropad direkt (%s) ERROR Element.renderView anropad direkt (%s) ERROR: Block.renderViewContent anropad direkt Redigera Huvudfokus Wikipedia-artikel Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Skriv en ledtråd här. Lämna
 fältet blankt om Du inte vill ge någon ledtråd Skriv en ledtråd här. Lämna
 fältet blankt om Du inte vill ge någon ledtråd Fyll i alla detaljer om den aktivitet som eleven
 skall reflektera över. Fyll i den feedback du vill ge eleven i feedbackfältet. Fältet kan även lämnas blankt. Fyll i den feedback du vill ge eleven i feedbackfältet. Fältet kan även lämnas blankt. Skriv instruktioner här Skriv in varje alternativ som eleven kan välja
 från i respektive val-box. Du kan lägga till val genom att klicka
 på "LÄGG TILL NYTT ALTERNATIV" -knappen. Du kan ta bort alternativ genom
 att klicka på
 "x" intill respektive alternativ. Skriv in varje alternativ som eleven kan välja
 från i respektive val-box. Du kan lägga till val genom att klicka
 på "LÄGG TILL NYTT ALTERNATIV" -knappen. Du kan ta bort alternativ genom
 att klicka på
 "x" intill respektive alternativ. Fyll i referenser till det valda inläsningsmaterialet.
 Utformningen av referenser kan göras baserad på den referensstil som används i din organisation. Skriv instruktioner här Skriv en titel här Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Rubrik: Exporterad till %s iDevicePane Exporterad till %s Extern webbplats label="IMS Content Package" accesskey="i" label="IMS Content Package" accesskey="i" Rätt Textfält Falskt Falskt Återkoppling Återkoppling Filnamn %s är en fil, kan inte ersätta den Byt namn Flash Flash med text Float: Fritext Wikipedia-artikel Rubrik: Rubrik: Rubrik: Wikipedia-artikel Visa resultat Wikipedia-artikel Artikel Artikel Göm Visa/Göm återkoppling Dölj/visa ord Ledtråd Ledtråd Hem Rubrik: iDevice Editor Ta bort fråga iDevice Editor label="IMS Content Package" accesskey="i" Ikoner Bildgalleri Bild Bildgalleri Bildgalleri Bild med text iDevicePane Fel Fel! debugInfo Instruktioner label="IMS Content Package" accesskey="i" Instruktioner Instruktioner för elever Rubrik: Rubrik: Rubrik: Wikipedia-artikel Wikipedia-artikel Rubrik: Rubrik: Etikett Välj språk Rubrik: Etikett Rubrik: Vänster Nivå 1: Nivå 2: Nivå 3: Välj språk Rubrik: Ladda Lägg till bilder Filnamn %s är en fil, kan inte ersätta den Rubrik: Wikipedia-artikel Falskt Artikel Artikel Artikel Rubrik: Rubrik: Flytta ner Inga bilder har laddats Inga bilder har laddats Flytta upp Flytta ner Flytta upp Fleralternativsfråga N Namn Visa Ny iDevice Text Inga bilder har laddats Ingen förhandsgranskning tillgänglig. Kan inte förminska bilden Ingen förhandsgranskning tillgänglig. Kan inte förminska bilden Ingen förhandsgranskning tillgänglig. Kan inte förminska bilden Inget huvudfokus Ingen Rubrik: Rubrik: Rubrik: O OK Mål Målsättning beskriver det förväntade reultatet av momentet och bör
 definiera vad eleverna skall kunna när
 de avslutat momentet Val Val Översikt label="IMS Content Package" accesskey="i" Projektets egenskaper Paketet har sparats till: %s Paketet har sparats till: %s Rubrik: Pedagogisk hjälp Pedagogiska tips Rubrik: Skriv in ett iDevicenamn Skriv in ett iDevicenamn Välj bild. Välj bild. Välj ett korrekt svar för varje fråga Wikipedia-artikel Wikipedia-artikel Inställningar Förkunskaper Förkunskaper refererar till den kunskap som eleven bör ha sedan tidigare
 för att klara av ett moment. Exempel på förkunskaper är: <ul>
<li> Eleven måste kunna lösa en andragradsekvation </li>
<li> Eleven skall ha känna till grundläggande fonetik </li>
</ul>
 Förhandsgranska Filnamn %s är en fil, kan inte ersätta den Förhandsgranska Projektets egenskaper Projektets titel: Egenskaper Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Tillåt automatiska förslag (auto completion)
 när användaren fyller i. Syfte Fyll i elevinstruktioner Fråga Fråga: Skicka svar R Läs nedanstående stycke och fyll i de ord som saknas Läs nedanstående stycke och fyll i de ord som saknas Läsaktivitet Läsaktivitet Reflektion Reflektion är en undervisningsmetod som ofta används
 för att koppla samman teori och praktik. Reflekterande uppgifter ger eleven
 en möjlighet att reflektera över sina egna observationer innan dessa rapporteras.
 Journaler, dagböcker, profiler och portföljer är
 användbara verktyg för att samla observationsdata. Rubriker och guider kan vara effektiva verktyg för feedback. Reflektiv fråga Reflektion Reflektion Byt namn Höger Höger Rubrik: Rubrik: S SCORM-test experimentellt SCORM-test experimentellt SCORM-test experimentellt SKICKA SVAR Visa Rubrik: Spara Sektion Välj Flashfil Välj bild Välj språk Välj språk Välj fil Välj bild Välj fil Välj Flashfil Välj fil Välj språk Välj språk Välj fil Välj bild Välj bild För att ange rätt svar, klicka på 
 radioknappen intill det rätta svaret Välj nivå för godkänd Rubrik: Visa Välj bild %s Visa/göm svar Visa/Göm återkoppling Välj bild %s Visa/göm svar Visa/Göm återkoppling Ledtråd Falskt titel Visa Rubrik: Wikipedia-artikel Rubrik: Sektion Rubrik: Delfokus Tyvärr, fel filformat Tyvärr, fel filformat Tyvärr, fel filformat:
%s Story: Wikipedia-artikel Story: Ämne: Skicka svar Skicka svar Falskt Rubrik: Wikipedia-artikel Rubrik: Artikel Artikel Rubrik: Taxonomi Text Textbox Textfält Artikel Bifoga-iDevicen används för att bifoga existerande filer till ditt .elp innehåll. Du kan till exempel bifoga en PDF-fil eller en PPT-presentation som du vill ge eleverna tillgång till. Dessa kan bifogas och ges en märkning som indikerar deras innehåll och storlek. Eleverna kan klicka på den bifogade filen för att ladda ner den. Huvudelen av en lärresurs kommer att användas
 för att beskriva sammanhang, ge instruktioner och tillhandahålla generell information.
 Detta utgörett ramverk inom vilket de pedagogiska
 aktiviteterna skapas.  Beskriv målet för denna resurs, Projektets titel: Syftedialogen låter dig beskriva ditt tänkta syfte med dina iDevicer för andra potentiella användare Detta är ett fritextfält där allmänt utbildningsinnehåll kan fyllas i. Detta är ett valfritt fält Detta är ett valfritt fält Detta kommer att radera denna iDevice.\ Vill du verkligen göra det? Rubrik: Tips: titel titel För att ange rätt svar, klicka på 
 radioknappen intill det rätta svaret Visa Ämne Sant Sant -eller falsktfråga Sant/falskt-frågor presenterar ett påstående som eleven skall ta ställning till. Denna typ av frågor kan ibland lämpa sig
 för faktafrågor ellerfrågor som kan besvaras med antingen eller. Visa Falskt Artikel Diskussionsämne: Fyll i den feedback du vill ge eleven när denne väljer en viss fråga. Om du inte fyller i
 detta fält kommer eXe automatiskt att fylla i: 
"Rätt svar" som indikation på val av rätt svar, respektive 
"Fel svar" för alla andra alternativ. Fyll i den feedback du vill ge eleven när denne väljer en viss fråga. Om du inte fyller i
 detta fält kommer eXe automatiskt att fylla i: 
"Rätt svar" som indikation på val av rätt svar, respektive 
"Fel svar" för alla andra alternativ. Beskriv målet för denna resurs, Tips: U Rubrik: Enhet University of Auckland Ladda Detta element kan användas flexibelt. Använd det för att ge en summering
 inläsningmaterialet eller som en utgångspunkt för fortsatt analys av
 materialet genom att presentera en frågeställning eller ett påstående som kan
 utgöra grunden för en debatt. Använd denna iDevice om Du har många bilder att visa Ett pedagogiskt tips låter dig beskriva den pedagogiska intentionen och tanken bakom utvecklingen av dina iDevicer. VK_DELETE VK_INSERT VK_LEFT VK_RIGHT Byt namn titel Vad skall läsas Wikibokartiklar Wikibokartiklar Visa Din nya iDevice kommer att visas i iDevice-panelen med denna titel.  Artikel Lämna blank för originalstorlek debugInfo eXe eXe : XHTML editor för e-lärande Bilaga %s har ingen föräldranod http://en.wikipedia.org/ iDevice %s har inget paket iDevice Editor iDevice Editor iDevicePane iDevicePane label="Om" accesskey="a" label="Enstaka sida" accesskey="p" label="Exportera" accesskey="e" label="IMS Content Package" accesskey="i" label="Fil" accesskey="f" label="Hjälp" accesskey="h" label="Hjälp" accesskey="h" label="IMS Content Package" accesskey="i" label="Exportera" accesskey="e" label="IMS Content Package" accesskey="i" label="Öppna" accesskey="o" label="Ny" accesskey="n" label="Öppna" accesskey="o" label="Om" accesskey="a" label="Om" accesskey="a" label="Uppdatera" accesskey="r" label="Uppdatera" accesskey="r" label="Ny" accesskey="n" label="Exportera" accesskey="e" label="SCORM 1.2" accesskey="s" label="Spara som..." accesskey="a" label="Spara" accesskey="s" label="Enstaka sida" accesskey="p" label="Stilar" accesskey="s" label="Fil" accesskey="f" label="Verktyg" accesskey="t" label="Fil" accesskey="f" label="Fil" accesskey="f" label="Webbplats" accesskey="w" label="Öppna" accesskey="o" label="Öppna" accesskey="o" label="Webbplats" accesskey="w" label="Ny" accesskey="n" Rubrik: outlinePane Text 